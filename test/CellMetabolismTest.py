__author__ = 'srb'

import unittest
import tumorsimulation.CellMetabolism
from tumorsimulation.Cell import CellClass

class TestSequenceFunctions(unittest.TestCase):

    def setUp(self):
        CellClass.AEROBIC
        CellClass.ANAEROBIC
        CellClass.QUIESCENCE
        CellClass.PROLIFERATION
        CellClass.MIGRATIVE

    def test_calculateO2ConsumptionForCell(self):
        '''
            Testing oxygen consumption calculation for aerobic state during
            proliferation
        '''
        realResult = 10 * 1.5 * 9;
        calculatedResult = tumorsimulation.CellMetabolism.calculateO2ConsumptionForCell(CellClass.AEROBIC,
                                                     CellClass.PROLIFERATION,
                                                     10, 10**5)
        
        self.assertEqual(realResult,calculatedResult)

    def test_calculateGlucoseConsumptionForCell(self):
        '''
            Testing glucose consumption calculation for aerobic state during
            proliferation
        '''
        realResult = 10 * 1.5 * 9;
        calculatedResult =\
        tumorsimulation.CellMetabolism.calculateGlucoseConsumptionForCell(CellClass.AEROBIC,
                                                     CellClass.PROLIFERATION,
                                                     10,10, 10**5)
        
        self.assertEqual(realResult,calculatedResult)


    def test_calculateAcidProductionForCell(self):
        '''
            Testing acid consumption calculation for anaerobic state during
            proliferation
        '''
        realResult = 10 * 1.5 * 9;
        calculatedResult =\
        tumorsimulation.CellMetabolism.calculateAcidProductionForCell(CellClass.ANAEROBIC,
                                                     CellClass.PROLIFERATION,
                                                     10)
        
        self.assertEqual(realResult,calculatedResult)
if __name__ == '__main__':
    unittest.main()
