"""o2calculator Test Module"""

__author__ = 'Serbulent Unsal'


# pylint:disable=import-error,too-many-public-methods,invalid-name,attribute-defined-outside-init,wrong-import-position,wrong-import-order
import matplotlib as mpl
mpl.use('Agg')
import unittest
import itertools
import math
import numpy
from fipy import numerix
from tumorsimulation.o2calculator import o2CalculatorClass
from tumorsimulation.ConfigModule import ConfigClass
from tumorsimulation.NonDimensionalizer import NonDimensionlizerClass


class TestSequenceFunctions(unittest.TestCase):
    """
    o2calculator Test Class
    This class test o2 diffusion equation at external and internal boundaries
    """

    def setUp(self):
        """init before test"""
        configObject = ConfigClass()
        nonDimensionalizer = NonDimensionlizerClass(configObject)
        self.o2Calculator = o2CalculatorClass(nonDimensionalizer, configObject)

        self.sizex = 400
        self.sizey = 400

        self.o2consumptionMatrix = numpy.zeros((self.sizex, self.sizey))
        self.o2FlowMatrix = numpy.zeros((self.sizex, self.sizey))

        from random import randint
        radiusOfTumor = 160
        centerX = int(self.sizex / 2)
        centerY = int(self.sizey / 2)
        for x in range(int((self.sizex / 2) - radiusOfTumor),
                       int((self.sizex / 2) + radiusOfTumor)):
            for y in range(int((self.sizey / 2) - radiusOfTumor),
                           int((self.sizey / 2) + radiusOfTumor)):
                distanceToCenter = int(math.hypot(x - centerX, y - centerY))
                chance = randint(1, 100)
                if chance >= 60 and distanceToCenter < radiusOfTumor:
                    self.o2consumptionMatrix[x][y] = \
                        nonDimensionalizer.nonDimensionalO2ConsRate * 1.5 * 9
        # create capilary matrix
        self.cor = ((50, 50), (60, 60), (70, 70), (80, 80), (90, 90), (100, 100),
                    (110, 110), (120, 120), (130, 130), (140, 140), (160, 160),
                    (170, 170), (230, 230), (240, 240), (250, 250), (260, 260),
                    (270, 270), (280, 280), (290, 290), (300, 300), (310, 310),
                    (320, 320), (330, 330), (340, 340), (350, 350))

        for x, y in self.cor:
            self.o2FlowMatrix[x][y] = 1.0

    def test_calculateO2Concentration(self):
        """
        test o2 diffusion equation at external and internal boundaries
        """

        consumptionChain = itertools.chain(*self.o2consumptionMatrix)
        flowChain = itertools.chain(*self.o2FlowMatrix)
        consumptionFlattenedMatrix = list(consumptionChain)
        flowFlattenedMatrix = list(flowChain)
        self.o2ConcentrationMatrix = []
        for t in range(5):
            print 'Oxygen diffusion test calculating time step: ' + str(t)
            self.o2ConcentrationMatrix = self.o2Calculator.calculateO2(
                consumptionFlattenedMatrix, t, flowFlattenedMatrix, True, True)
        # Check values at capilaries they should be 1.0
        for x, y in self.cor:
            targetCell1DCoordinates = (x * 400) + y
            # This will only printed if test fails
            print 'Coordinates: ' + str((x, y)) + 'Oxygen Concentration is: ' \
                + str(self.o2ConcentrationMatrix[targetCell1DCoordinates])
            self.assertTrue(numerix.allclose(
                self.o2ConcentrationMatrix[targetCell1DCoordinates], 1.0,
                rtol=1e-2, atol=1e-2))
            # !!!!!!!!!!!!!! it gives 0.79 ? Check this with Aybar Hoca
            # This phenomenon occurs even consumption is 0
            #print 'Coordinates: ' + str((x, y-1)) + ' Concentration is: ' \
            #    + str(self.o2ConcentrationMatrix[targetCell1DCoordinates-1])
            #self.assertTrue(numerix.allclose(
            #    self.o2ConcentrationMatrix[targetCell1DCoordinates - 1], 1.0,
            #    atol=1e-5))

        # Check values at borders they should be 0
        borders = ((0, 0), (self.sizex - 1, 0), (0, self.sizey - 1),
                   (self.sizex - 1, self.sizey - 1))
        for x, y in borders:
            targetCell1DCoordinates = (x * 400) + y
            # This will only printed if test fails
            print 'Coordinates: ' + str((x, y)) + 'Oxygen Concentration is: ' \
                + str(self.o2ConcentrationMatrix[targetCell1DCoordinates])
            self.assertTrue(numerix.allclose(self.o2ConcentrationMatrix[targetCell1DCoordinates],
                                             0.0, atol=1e-2))


if __name__ == '__main__':
    unittest.main()
