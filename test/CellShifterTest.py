__author__ = 'srb'

import unittest
import numpy
from tumorsimulation.CellShifter import CellShifterClass
from tumorsimulation.Cell import CellClass

class MockCellClass:
    
    def __init__(self,x,y):
        self.cellPositionX = x
        self.cellPositionY = y
        self.borderCell = False

class TestSequenceFunctions(unittest.TestCase):

    def setUp(self):
        '''
            Tumor cells created at the center of an 10x10 matrix in a 3x3 area
        '''
        self.cells = []
        self.sizex = 10
        self.sizey = 10
        self.cellTypeMatrix = numpy.zeros((self.sizex,self.sizey))

        for x in xrange(0, self.sizex):
            rowcells = []
            for y in xrange(0, self.sizey):
                c = MockCellClass(x,y)
                rowcells.append(c)
                if (x>=3 and x<6 and y>=3 and y < 6):
                    self.cellTypeMatrix[x][y] = CellClass.TUMOR_CELL
            self.cells.append(rowcells)

    def test_borderControl(self):
        '''
            Testing if function could determine border cells correctly
        '''
        cs = CellShifterClass(10,10,10,10,10)
        cs.borderControl(self.cellTypeMatrix, self.cells[3][3])    
        ''' A corner cell should be treated as border cell  '''
        self.assertTrue(self.cells[3][3].borderCell)
        ''' A cell which stands at middile part should not be treated as border
        cell'''
        self.assertFalse(self.cells[3][2].borderCell)

if __name__ == '__main__':
    unittest.main()
