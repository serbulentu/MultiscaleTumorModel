__author__ = 'srb'

import unittest
from ConfigModule import ConfigClass
from NonDimensionalizer import NonDimensionlizerClass

class TestSequenceFunctions(unittest.TestCase):

    def setUp(self):
        configObject = ConfigClass()
        self.nonDimensionalizer = NonDimensionlizerClass(configObject)
        self.time = 16
        self.length = 1
        self.n0 = 1/0.0025**2
        self.nonDimensionalDc = 1.0368
        self.nonDimensionalRc= 124.6871
        self.nonDimensionalDg = 5.2416
        self.nonDimensionalRg= 489.1569
        self.nonDimensionalDh = 0.6336
        self.nonDimensionalRh= 138240

    def test_nonDimensionlizeDiffusionConstant(self):
        Dc = 1.8*10**-5
        t = self.time
        L = self.length

        testResult = self.nonDimensionalizer.nonDimensionlizeDiffusionConstant(Dc,t,L)
        self.assertTrue(abs(self.nonDimensionalDc-testResult) < 1e-5)

    def test_nonDimensionlizeConsumptionOrProductionRate(self):
        t = self.time
        n0 = self.n0
        rc = 2.3*10**-16
        c0 = 1.7*10**-8

        testResult = self.nonDimensionalizer.nonDimensionlizeConsumptionOrProductionRate(t,n0,rc,c0)
        self.assertTrue(abs(self.nonDimensionalRc-testResult) < 1e-3)

    def test_accessControlForNondimensionalizedVariables(self):
        print "self.nonDimensionalizer.nonDimensionalO2ConsRate: " + str(self.nonDimensionalizer.nonDimensionalO2ConsRate )
        print "self.nonDimensionalRc: " + str(self.nonDimensionalRc)
        self.assertTrue(abs(self.nonDimensionalRc-self.nonDimensionalizer.nonDimensionalO2ConsRate) < self.nonDimensionalRc/100)
        self.assertTrue(abs(self.nonDimensionalDc-self.nonDimensionalizer.nonDimensionalO2DiffConst) < self.nonDimensionalDc/100)
        self.assertTrue(abs(self.nonDimensionalRg-self.nonDimensionalizer.nonDimensionalAnaerobicGlucoseConsRate) < self.nonDimensionalRg/100)
        self.assertTrue(abs(self.nonDimensionalDg-self.nonDimensionalizer.nonDimensionalGlucoseDiffConst) < self.nonDimensionalDg/100)
        self.assertTrue(abs(self.nonDimensionalRh-self.nonDimensionalizer.nonDimensionalHIyonProdRate) < self.nonDimensionalRh/100)
        self.assertTrue(abs(self.nonDimensionalDh-self.nonDimensionalizer.nonDimensionalHIyonDiffConst) < self.nonDimensionalDh/100)


if __name__ == '__main__':
    unittest.main()
