__author__ = 'srb'

import unittest
import numpy
import itertools
import math
import tumorsimulation.InvasionSubSystem as InvasionSubSystem
import tumorsimulation.PhCalculatorCython as PhCalculatorCython
from tumorsimulation.Cell import CellClass
from ConfigModule import ConfigClass

class MockCellClass:

    def __init__(self, x, y):
        self.cellPositionX = x
        self.cellPositionY = y
        self.borderCell = False

class TestSequenceFunctions(unittest.TestCase):

    def setUp(self):
        '''
            Tumor cells created at the center of an 10x10 matrix in a 3x3 area
        '''
        self.cells = []
        self.sizex = 10
        self.sizey = 10
        self.cellTypeMatrix = numpy.zeros((self.sizex, self.sizey))
        self.cellStressScoreMatrix = numpy.zeros((self.sizex,self.sizey))

        for x in xrange(0, self.sizex):
            rowcells = []
            for y in xrange(0, self.sizey):
                c = MockCellClass(x, y)
                rowcells.append(c)
                if (x >= 3 and x < 6 and y >= 3 and y < 6):
                    self.cellTypeMatrix[x][y] = CellClass.TUMOR_CELL
                    # Write an arbitrary cell score for all non empty cells
                    self.cellStressScoreMatrix[x][y] = 0.9
            self.cells.append(rowcells)

    def test_getOptimalAcidityScoreForInvasion(self):
        '''
            Testing if function could calculate optimal acidity 
        '''
        candidateCellOptimalPh = 6.80000019073
        candidateCellAreaAcidConcentration =  6.03022849405e-12
        minPh = 5.5
        maxPh = 8.0
        coefficentForMiliMolartoMol = 6.75e-08

        score1 = InvasionSubSystem.getOptimalAcidityScoreForInvasion(candidateCellOptimalPh,
                                                                     candidateCellAreaAcidConcentration,
                                                                     coefficentForMiliMolartoMol)
        # For explantion look original function
        phDiff = abs(candidateCellOptimalPh - 7.04897000433)
        if phDiff <= 0.1:
            score2 = 1
        else:
            score2 = ((1 / phDiff) - 0.3) / (10 - 0.3)

        # This will print only if test fails
        print "score1: " + str(score1)
        print "score2: " + str(score2)
        self.assertEquals(round(score1,4), round(score2,4))

    def test_findMostSuitableNeighbourToInvade(self):
        ''' Test function to migrate cell from 2,2 to 1,1 '''
        # Set invasion score for migration
        invasionCause = 2
        # Set Cell metabolism to Aerobic
        cellMetabolism = CellClass.AEROBIC
        x = 3
        y = 3
        o2ConcentrationMatrix = numpy.zeros((self.sizex, self.sizey))
       # print(len(o2ConcentrationMatrix))
        o2ConcentrationMatrix[2][2] = 0.5
        glucoseConcentrationMatrix  = numpy.zeros((self.sizex, self.sizey))
        glucoseConcentrationMatrix[2][2] = 0.5
        o2chain = itertools.chain(*o2ConcentrationMatrix)
        o2flattenedMatrix = list(o2chain)
        #print o2flattenedMatrix[3]
        glucosechain = itertools.chain(*glucoseConcentrationMatrix)
        glucoseflattenedMatrix = list(glucosechain)
        # Since cell metabolism is aerobic we will not consider acid
        # concentration so we create a mock variable
        acidConcentrationMatrix = numpy.zeros((self.sizex, self.sizey))
        acidchain = itertools.chain(*acidConcentrationMatrix)
        acidflattenedMatrix = list(acidchain)
        phBackground = ConfigClass.phBackground
        proliferationStressTreshold = ConfigClass.cellStressScoreThreshold
        # Check InvasionSubSystem class for this
        timeStep = 1
        cmo = 0.2
        cmg = 0.3
        cmh = 0.3
         # Since cell metabolism is aerobic we will not consider anaerobic coeffs
        cmg_an = 0
        cmh_an = 0
        coefficentForMiliMolartoMol = 6.75e-08
        
        coor = InvasionSubSystem.findMostSuitableNeighbourToInvade(self.cellStressScoreMatrix,
                                                            self.cellTypeMatrix,
                                                            cellMetabolism,
                                                            x,y,
                                                            o2flattenedMatrix,
                                                            glucoseflattenedMatrix,
                                                            acidflattenedMatrix,
                                                            phBackground,
                                                            proliferationStressTreshold,
                                                            timeStep,
                                                            invasionCause, cmo, cmg, cmh,
                                                            cmg_an,  cmh_an,
                                                            coefficentForMiliMolartoMol,1)
        
        print 'coor: ' + str(coor)
        self.assertTrue(coor == (2,2))


if __name__ == '__main__':
    unittest.main()
