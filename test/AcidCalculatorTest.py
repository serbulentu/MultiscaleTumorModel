"""NutritionCalculator Test Module"""

__author__ = 'Serbulent Unsal'


# pylint:disable=import-error,too-many-public-methods,invalid-name,attribute-defined-outside-init,wrong-import-position,wrong-import-order
import matplotlib as mpl
mpl.use('Agg')
import unittest
import itertools
import math
import numpy
from fipy import numerix
from tumorsimulation.AcidCalculator import AcidCalculatorClass
from tumorsimulation.ConfigModule import ConfigClass
from tumorsimulation.NonDimensionalizer import NonDimensionlizerClass


class TestSequenceFunctions(unittest.TestCase):
    """
    AcidCalculator Test Class
    This class test glucose diffusion equation at external and internal boundaries
    """

    def setUp(self):
        """init before test"""
        configObject = ConfigClass()
        nonDimensionalizer = NonDimensionlizerClass(configObject)
        self.acidCalculator = AcidCalculatorClass(
            nonDimensionalizer, configObject)

        self.sizex = 400
        self.sizey = 400

        self.acidProductionMatrix = numpy.random.random(
            (self.sizex, self.sizey))
        self.acidProductionMatrix = numpy.zeros((self.sizex, self.sizey))
        self.acidFlowMatrix = numpy.zeros((self.sizex, self.sizey))

        from random import randint
        radiusOfTumor = 160
        centerX = int(self.sizex / 2)
        centerY = int(self.sizey / 2)
        for x in range(int((self.sizex / 2) - radiusOfTumor),
                       int((self.sizex / 2) + radiusOfTumor)):
            for y in range(int((self.sizey / 2) - radiusOfTumor),
                           int((self.sizey / 2) + radiusOfTumor)):
                distanceToCenter = int(math.hypot(x - centerX, y - centerY))
                chance = randint(1, 100)
                if chance >= 60 and distanceToCenter < radiusOfTumor:
                    self.acidProductionMatrix[x][y] = 10  # \

    def test_calculateAcidConcentration(self):
        """
        test H+ diffusion equation at external and internal boundaries
        """
        productionChain = itertools.chain(*self.acidProductionMatrix)
        productionFlattenedMatrix = list(productionChain)
        self.acidConcentrationMatrix = []
        for t in range(2):
            print 'Acid diffusion test calculating time step: ' + str(t)
            self.acidConcentrationMatrix = self.acidCalculator.calculateAcid(
                productionFlattenedMatrix, t, False)
        # Check values at capilaries they should be 1.0
        targetCell1DCoordinates = (0 * 400) + 100
        # This will only printed if test fails
        print 'Coordinates: 0,100 Acid Concentration is: ' \
            + str(self.acidConcentrationMatrix[targetCell1DCoordinates])
        self.assertTrue(numerix.allclose(
            self.acidConcentrationMatrix[targetCell1DCoordinates], 1.0,
            rtol=1e-2, atol=1e-2))


if __name__ == '__main__':
    unittest.main()
