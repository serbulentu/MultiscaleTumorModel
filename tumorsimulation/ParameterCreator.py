__author__ = 'srb'

import random
import sqlite3

class ParameterCreatorClass():

    def __init__(self):
        self.angioMin = 0
        self.angioMax = 0
        self.Cmin = 0
        self.Cmax= 0
        with sqlite3.connect('tumordb') as conn:
            cursor = conn.cursor()
            primaryKey =1
            cursor.execute("""
                  SELECT angio_percent_min,angio_percent_max,Cmin,Cmax FROM coefficients WHERE coeff_pk = %s
                  """ % primaryKey)
            self.angioMin, self.angioMax, self.Cmin, self.Cmax = cursor.fetchone()
            self.Cmin = self.Cmin *10
            self.Cmax = self.Cmax*10
    
    def controlledRandomInt(self, minVal, maxVal ):
	    if (minVal>=maxVal):
		    return maxVal
	    else:
		    return random.randint(minVal,maxVal)

    def createNewParameterFamily(self,coeffsOld):

        Cmo_old,Cmg_old,Cmh_old,Cmg_an_old,Cmh_an_old,Cpo_old,Cpg_old,Cph_old,Cpg_an_old,Cph_an_old, angio_percent_old = [int(x*10) for x in coeffsOld]
        angio_percent_old = angio_percent_old/10

        #Select Migration Oxygen Coefficient Neighbour
        if Cmo_old-2 > self.Cmin:
            CmoNeighbourMin= Cmo_old-2
        else:
            CmoNeighbourMin = self.Cmin

        if Cmo_old+2 < self.Cmax:
            CmoNeighbourMax= Cmo_old+2
        else:
            CmoNeighbourMax = self.Cmax
        Cmo = self.controlledRandomInt(CmoNeighbourMin, CmoNeighbourMax)

        #Select Migration Glucose Coefficient Neighbour
        if Cmg_old-2 > self.Cmin:
            CmgNeighbourMin= Cmg_old-2
        else:
            CmgNeighbourMin = self.Cmin

        if Cmg_old+2 < 10-Cmo:
            CmgNeighbourMax= Cmg_old+2
        else:
            CmgNeighbourMax = 10-Cmo-1
        Cmg = self.controlledRandomInt(CmgNeighbourMin,CmgNeighbourMax)


        #Select Migration Hydrogen Ion Coefficient Neighbour
        if Cmh_old-2 > self.Cmin:
            CmhNeighbourMin= Cmh_old-2
        else:
            CmhNeighbourMin = self.Cmin

        if Cmh_old+2 < 10-Cmo-Cmg:
            CmhNeighbourMax= Cmh_old+2
        else:
            CmhNeighbourMax = 10-Cmo-Cmg
        if CmhNeighbourMax < CmhNeighbourMin:
            CmhNeighbourMin = CmhNeighbourMax
        Cmh = self.controlledRandomInt(CmhNeighbourMin,CmhNeighbourMax)

        #Select Migration Glucose Coefficient Neighbour for Anaerobic Cell Metabolism
        if Cmg_an_old-2 > self.Cmin:
            CmgAnaerobNeighbourMin= Cmg_an_old-2
        else:
            CmgAnaerobNeighbourMin = self.Cmin

        if Cmg_an_old+2 < self.Cmax:
            CmgAnaerobNeighbourMax= Cmg_an_old+2
        else:
            CmgAnaerobNeighbourMax = self.Cmax
        Cmg_an = self.controlledRandomInt(CmgAnaerobNeighbourMin, CmgAnaerobNeighbourMax)

        #Select Migration Hydrogen Ion  Coefficient Neighbour for Anaerobic Cell Metabolism
        if Cmh_old-2 > self.Cmin:
            CmhAnaerobNeighbourMin= Cmh_old-2
        else:
            CmhAnaerobNeighbourMin = self.Cmin

        if Cmh_old+2 < 10-Cmg_an:
            CmhAnaerobNeighbourMax= Cmh_old+2
        else:
            CmhAnaerobNeighbourMax = 10-Cmg_an
        Cmh_an = self.controlledRandomInt(CmhAnaerobNeighbourMin,CmhAnaerobNeighbourMax)

        #Select Proliferation Oxygen Coefficient Neighbour
        if Cpo_old-2 > self.Cmin:
            CpoNeighbourMin= Cpo_old-2
        else:
            CpoNeighbourMin = self.Cmin

        if Cpo_old+2 < self.Cmax:
            CpoNeighbourMax= Cpo_old+2
        else:
            CpoNeighbourMax = self.Cmax
        Cpo = self.controlledRandomInt(CpoNeighbourMin, CpoNeighbourMax)

        #Select Proliferation Glucose Coefficient Neighbour
        if Cpg_old-2 > self.Cmin:
            CpgNeighbourMin= Cpg_old-2
        else:
            CpgNeighbourMin = self.Cmin

        if Cpg_old+2 < 10-Cpo:
            CpgNeighbourMax= Cpg_old+2
        else:
            CpgNeighbourMax = 10-Cpo-1
        Cpg = self.controlledRandomInt(CpgNeighbourMin,CpgNeighbourMax)

        #Select Proliferation Hydrogen Ion Coefficient Neighbour
        if Cph_old-2 > self.Cmin:
            CphNeighbourMin= Cph_old-2
        else:
            CphNeighbourMin = self.Cmin

        if Cph_old+2 < 10-Cpo-Cpg:
            CphNeighbourMax= Cph_old+2
        else:
            CphNeighbourMax = 10-Cpo-Cpg
        if CphNeighbourMax < CphNeighbourMin:
            CphNeighbourMin = CphNeighbourMax
        Cph = self.controlledRandomInt(CphNeighbourMin,CphNeighbourMax)

        #Select Proliferation Glucose Coefficient Neighbour for Anaerobic Cell Metabolism
        if Cpg_an_old-2 > self.Cmin:
            CpgAnaerobNeighbourMin= Cpg_an_old-2
        else:
            CpgAnaerobNeighbourMin = self.Cmin

        if Cpg_an_old+2 < self.Cmax:
            CpgAnaerobNeighbourMax= Cpg_an_old+2
        else:
            CpgAnaerobNeighbourMax = self.Cmax
        Cpg_an = self.controlledRandomInt(CpgAnaerobNeighbourMin, CpgAnaerobNeighbourMax)

        #Select Proliferation Hydrogen Ion Coefficient Neighbour for Anaerobic Cell Metabolism
        if Cph_old-2 > self.Cmin:
            CphAnaerobNeighbourMin= Cph_old-2
        else:
            CphAnaerobNeighbourMin = self.Cmin

        if Cph_old+2 < 10-Cpg_an:
            CphAnaerobNeighbourMax= Cph_old+2
        else:
            CphAnaerobNeighbourMax = 10-Cpg_an
        Cph_an = self.controlledRandomInt(CphAnaerobNeighbourMin,CphAnaerobNeighbourMax)

        #Select Vascularisation Percent Neighbour
        if angio_percent_old-5 > self.angioMin:
            angioNeighbourMin= angio_percent_old-5
        else:
            angioNeighbourMin = self.angioMin

        if angio_percent_old+5 < self.angioMax+1:
            angioNeighbourMax= angio_percent_old+5
        else:
            angioNeighbourMax = self.angioMax+1
        angio_percent = self.controlledRandomInt(angioNeighbourMin,angioNeighbourMax)

        #Since all coeffs will divided by 10 we multiply angio_percent by 10 before return
        coeffs = Cmo,Cmg,Cmh,Cmg_an,Cmh_an,Cpo,Cpg,Cph,Cpg_an,Cph_an, int(angio_percent*10)
        return [float(x)/10 for x in coeffs]

    def createCapillary(self, angio_percent):
        import random
        capilaryList = []
        for i in range(400*400*int(angio_percent)/100000):
            x = random.randint(0, 399)
            y = random.randint(0, 399)
            #If the cell is already a capilary then find a new location which is not a capilary cell
            while((x,y) in capilaryList):
                x = random.randint(0, 399)
                y = random.randint(0, 399)
            capilaryList.append((x,y))
        return capilaryList

    def createCapillaryNeighbourFamilyForOptimization(self,oldCapilaryList,neighbourRange):
        import random
        capilaryList = []
        for x,y in oldCapilaryList:
            x = self.controlledRandomInt(x-neighbourRange, x+neighbourRange)
            y = self.controlledRandomInt(y-neighbourRange, y+neighbourRange)
            #If the cell is already a capilary then find a new location which is not a capilary cell
            while((x,y) in capilaryList):
                x = self.controlledRandomInt(x-neighbourRange, x+neighbourRange)
                y = self.controlledRandomInt(y-neighbourRange, y+neighbourRange)

            # Make sure that parameters stays in limits
            if x < 0:
                x= 0
            if x > 399:
                x=399
            if y < 0:
                y=0
            if y > 399:
                y = 399

            capilaryList.append((x,y))

        return capilaryList
