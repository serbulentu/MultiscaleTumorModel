__author__ = 'Serbulent Unsal'

import pygame
import os
from ConfigModule import ConfigClass
from CellularGrid import CellularGrid


class Simulator():

    def main(self, isStandalone, deadline,coeffs,temp,capilaryList):
        configObject = ConfigClass()

        if configObject.disableXServer == True:
            os.environ["SDL_VIDEODRIVER"] = "dummy"

        nx = configObject.nx
        cellSize = configObject.cellSize

        realDimensionX=nx
        realDimensionY=realDimensionX

        pygame.init()
        realDimension = (realDimensionX,realDimensionY)
        if configObject.disableXServer == True:
            screen = pygame.display.set_mode(realDimension, pygame.NOFRAME)
        else:
            screen = pygame.display.set_mode(realDimension)
        screen.fill((0,0,0))

        gridSizeX = realDimensionX/cellSize
        gridSizeY = realDimensionY/cellSize

        self.grid = CellularGrid(screen, cellSize, gridSizeX, gridSizeY, isStandalone, deadline, coeffs,temp,capilaryList)

        if isStandalone:
            while True:
                # display will be updated in Cell Class
                self.grid.start()
        else:
            growthSteps = self.grid.start()
            return growthSteps

if __name__ == "__main__":
    sim = Simulator()
    sim.main(True, None, None,None,None)




