__author__ = 'srb'

import sqlite3
import numpy as np
import scipy.interpolate
import warnings
import random
from sklearn.metrics import mean_squared_error
from SimulationMain import Simulator
from ConfigModule import ConfigClass
from ParameterCreator import ParameterCreatorClass


with warnings.catch_warnings():
    warnings.filterwarnings("ignore")
    warnings.simplefilter('ignore', DeprecationWarning)


class TumorOptimizer():
    def __init__(self):
        self.initialCoeffs = self.getCoefficients(0)
        self.simulator = Simulator()
        self.originalTime = []
        self.originalVolume = []
        self.configObject = ConfigClass()
        self.angioMin = 0
        self.angioMax = 0
        self.Cmin = 0
        self.Cmax= 0
        parameterCreatorClassInstance = ParameterCreatorClass()
        self.createNewParameterFamily = parameterCreatorClassInstance.createNewParameterFamily
        self.createCapillary = parameterCreatorClassInstance.createCapillary
        self.createCapillaryNeighbourFamilyForOptimization = parameterCreatorClassInstance.createCapillaryNeighbourFamilyForOptimization

        with sqlite3.connect('tumordb') as conn:
            cursor = conn.cursor()
            #coeff_pk=0 has initial values
            cursor.execute("""SELECT days, tumor_volume FROM original_growth""")
            for row in cursor:
                self.originalTime.append(row[0] * 24)
                self.originalVolume.append(row[1])
            #Get min max values for parameters
            primaryKey =1
            cursor.execute("""
                  SELECT angio_percent_min,angio_percent_max,Cmin,Cmax FROM coefficients WHERE coeff_pk = %s
                  """ % primaryKey)
            self.angioMin, self.angioMax, self.Cmin, self.Cmax = cursor.fetchone()
            self.Cmin = self.Cmin *10
            self.Cmax = self.Cmax*10

        self.deadline = max(self.originalTime)/22
        self.deadline = self.deadline/2

    def main(self):

        with warnings.catch_warnings():
            warnings.filterwarnings("ignore")
            warnings.simplefilter('ignore', DeprecationWarning)

        initalCapillaryList = self.createInitialCapilaries(self.initialCoeffs)
        coeffsBest = self.startParameterOptimization(initalCapillaryList)
        capilaryListBest =  self.startCapillaryOptimization(coeffsBest,initalCapillaryList)

    def startParameterOptimization(self,initalCapillaryList):
        #print 'Parameter Optimization Started'
        with open("optimizationlog.txt", "a") as f: 
            f.write('\nParameter Optimization Started') 
        initialTemperature = 100
        # Calculate initial least square error
        coeffsBest = self.initialCoeffs
        #for temp in xrange(initialTemperature,0,-1):
        tumorGrowth = self.runSimulation(coeffsBest,'parameterOp_' + str(initialTemperature+1), initalCapillaryList)
        rmseBest = self.calculateFitness(tumorGrowth)
        #print 'rmseBest: ' + str(rmseBest)
        with open("optimizationlog.txt", "a") as f: 
            f.write('\nrmseBest: ' + str(rmseBest)) 
        for temp in xrange(initialTemperature,0,-5):
            coeffsNew = self.createNewParameterFamily(coeffsBest)
            tumorGrowth = self.runSimulation(coeffsNew,temp, initalCapillaryList)
            rmseNew = self.calculateFitness(tumorGrowth)
            #print 'rmseBest: ' + str(rmseBest)
            with open("optimizationlog.txt", "a") as f: 
                f.write('\nrmseBest: ' + str(rmseBest)) 
            #print 'rmseNew: ' + str(rmseNew)
            with open("optimizationlog.txt", "a") as f: 
                f.write('\nrmseNew: ' + str(rmseNew)) 
            if rmseNew < rmseBest:
                coeffsBest = coeffsNew
                rmseBest = rmseNew
             #   print 'New sheriff in town: ' + str(coeffsBest)
                with open("optimizationlog.txt", "a") as f: 
                    f.write('\nNew sheriff in town: ' + str(coeffsBest))
            else:
                # exp(-3) equals 0.004 this will be minimum acceptance probability at initial temp
                normalizationFactor = (rmseNew+rmseBest)
                deltaE= (rmseNew -rmseBest) / normalizationFactor
                #print 'deltaE: ' + str(deltaE)
                # initialTemperature is used as normalization factor
                #print 'initialTemperature: ' + str(initialTemperature)
                t = float(temp)/float(initialTemperature)
                #print 't :' + str(t)
                # second t used to decrease probability according to time decrease ratio is between 1 to 0
                acceptanceProbability = np.exp(-deltaE/t)*t
                #if acceptanceProbability > 1.0:
                    #print 'normalizationFactor: ' + str(normalizationFactor)
                    #print 'temp: ' + str(temp)
                    #print 't: ' + str(t)
                    #print 'deltaE: ' + str(deltaE)
                q = random.random()
                #print 'acceptanceProbability: ' + str(acceptanceProbability)
                #print 'q: ' + str(q)
                if q<acceptanceProbability:
                    coeffsBest = coeffsNew
                    rmseBest = rmseNew
                    #print 'New sheriff in town: ' + str(coeffsBest)
                    with open("optimizationlog.txt", "a") as f: 
                        f.write('\nNew sheriff in town: ' + str(coeffsBest))
        return coeffsBest

    def startCapillaryOptimization(self,coeffs,initalCapillaryList):
        #print 'Capillary Optimization Started'
        with open("optimizationlog.txt", "a") as f: 
            f.write( '\nCapillary Optimization Started')
        bestCapillaryList = initalCapillaryList
        neighbourRange = 10
        initialTemperature = 100
        tumorGrowth = self.runSimulation(coeffs,initialTemperature+1, bestCapillaryList)
        rmseBest = self.calculateFitness(tumorGrowth)
        #print 'rmseBest: ' + str(rmseBest)
        with open("optimizationlog.txt", "a") as f: 
            f.write('\nrmseBest: ' + str(rmseBest)) 
        for temp in xrange(initialTemperature,0,-5):
            newCapillaryList = self.createCapillaryNeighbourFamilyForOptimization(bestCapillaryList,neighbourRange)
            tumorGrowth = self.runSimulation(coeffs, 'capilaryOp_' + str(temp), newCapillaryList)
            rmseNew = self.calculateFitness(tumorGrowth)
            #print 'rmseBest: ' + str(rmseBest)
            with open("optimizationlog.txt", "a") as f: 
                f.write('\nrmseBest: ' + str(rmseBest)) 
            #print 'rmseNew: ' + str(rmseNew)
            with open("optimizationlog.txt", "a") as f: 
                f.write('\nrmseNew: ' + str(rmseNew)) 
            if rmseNew < rmseBest:
                bestCapillaryList = newCapillaryList
                rmseBest = rmseNew
                #print 'New sheriff in town bestCapillaryList: ' + str(bestCapillaryList)
                with open("optimizationlog.txt", "a") as f: 
                    f.write('\nNew sheriff in town bestCapillaryList: ' +
                            str(bestCapillaryList))
            else:
                # exp(-3) equals 0.004 this will be minimum acceptance probability at initial temp
                normalizationFactor = (rmseNew+rmseBest)
                deltaE= (rmseNew -rmseBest) / normalizationFactor
                #print 'deltaE: ' + str(deltaE)
                # initialTemperature is used as normalization factor
                t = float(temp)/float(initialTemperature)
                #print 't :' + str(t)
                acceptanceProbability = np.exp(-deltaE/t)*t
                if acceptanceProbability > 1.0:
                    pass
#                    print 'normalizationFactor: ' + str(normalizationFactor)
#                    print 'temp: ' + str(temp)
#                    print 't: ' + str(t)
#                    print 'deltaE: ' + str(deltaE)
                q = random.random()
#                print 'acceptanceProbability: ' + str(acceptanceProbability)
#                print 'q: ' + str(q)
                if q<acceptanceProbability:
                    bestCapillaryList = newCapillaryList
                    rmseBest = rmseNew
                    #print 'New sheriff in town bestCapillaryList: ' + str(bestCapillaryList)
                    #print 'Coeffs: ' + str(coeffs)
                    with open("optimizationlog.txt", "a") as f: 
                        f.write('\nNew sheriff in town bestCapillaryList: ' + str(bestCapillaryList))


        '''
            Finds best capilary positions from 10 different position option which will used as initial position in optimization
        '''
    def createInitialCapilaries(self,coeffs):
        #print 'Initial Capillary Creation Started'
        with open("optimizationlog.txt", "w") as f: 
            f.write('\nInitial Capillary Creation Started') 
        step = 10
        capilaryListBest = self.createCapillary((self.angioMin + self.angioMax)/2)
        with open("optimizationlog.txt", "a") as f: 
            f.write('\nFirst Capillary: ' + str(capilaryListBest))
        tumorGrowth = self.simulator.main(False, self.deadline, coeffs,step+1,capilaryListBest)
        rmseBest = self.calculateFitness(tumorGrowth)
        #print 'rmseBest: ' + str(rmseBest)
        with open("optimizationlog.txt", "a") as f: 
            f.write('\nrmseBest: ' + str(rmseBest)) 
        for step in xrange(step,0,-1):
            capilaryListNew = self.createCapillary((self.angioMin + self.angioMax)/2)
            tumorGrowth = self.simulator.main(False, self.deadline, coeffs,step,capilaryListNew)
            rmseNew = self.calculateFitness(tumorGrowth)
            #print 'rmseBest: ' + str(rmseBest)
            #print 'rmseNew: ' + str(rmseNew)
            with open("optimizationlog.txt", "a") as f: 
                f.write('\nrmseBest: ' + str(rmseBest)) 
            with open("optimizationlog.txt", "a") as f: 
                f.write('\nrmseNew: ' + str(rmseNew)) 
            if rmseNew < rmseBest:
                capilaryListBest = capilaryListNew
                rmseBest = rmseNew
                #print 'capilaryList updated in createInitialCapilaries function'
                with open("optimizationlog.txt", "a") as f: 
                    f.write('\ncapilaryList updated in createInitialCapilaries function')
        return capilaryListBest

    '''
        Calculates root of mean square error for tumor growth
    '''
    def calculateFitness(self,tumorGrowth):

        calculatedTumorGrowthTimeStepList = np.arange(0, (tumorGrowth.__len__()) * self.configObject.proliferationTime, self.configObject.proliferationTime)
        y_interp=scipy.interpolate.interp1d( calculatedTumorGrowthTimeStepList, tumorGrowth, kind='cubic')

        timeRange = np.arange(0,self.deadline*22,24)
        interPolatedTumorGrowth = y_interp(timeRange)
        y_interpOriginal = scipy.interpolate.interp1d( self.originalTime, self.originalVolume, kind='cubic')
        interPolatedOriginalTumorGrowth = y_interpOriginal(timeRange)

        return np.sqrt(mean_squared_error(interPolatedOriginalTumorGrowth, interPolatedTumorGrowth))

    def runSimulation(self, coeffs,temp, capilaryList):
        tumorGrowth = self.simulator.main(False, self.deadline, coeffs,temp,capilaryList)
        return tumorGrowth

    def getCoefficients(self, primaryKey):
        with sqlite3.connect('tumordb') as conn:
            cursor = conn.cursor()
            primaryKey =1
            #coeff_pk=1 has initial values
            cursor.execute("""
                  SELECT Cmo,Cmg,Cmh,Cmg_an,Cmh_an,Cpo,Cpg,Cph,Cpg_an,Cph_an, angio_percent FROM coefficients WHERE coeff_pk = %s
                  """ % primaryKey)
            return cursor.fetchone()

if __name__ == "__main__":
    optimizer =  TumorOptimizer()
    optimizer.main()
