__author__ = 'Serbulent Unsal'

import matplotlib as mpl
mpl.use('Agg')

import numpy
import pygame
import itertools
from time import *
import os
import warnings
import sqlite3

#import pyximport
#pyximport.install()

import tumorsimulation.CellMetabolism as CellMetabolism
from pylab import *
from Cell import CellClass
from o2calculator import o2CalculatorClass
from NutritionCalculator import NutritionCalculatorClass
from AcidCalculator import AcidCalculatorClass
from NonDimensionalizer import NonDimensionlizerClass
from ProliferationSubSystem import CellProliferationSubSystemClass
from ApoptosisSubSystem import CellApoptosisSubSystemClass
from EnergyMetabolismSubSystem import EnergyMetabolismSubSystemClass
from ConfigModule import ConfigClass
from MutationSubSystem import MutationSubSystemClass
from MigrationSubSystem import MigrationSubSystemClass
from MigrationPhenotypeDecisionSystem import MigrationPhenotypeDecisionSystemClass
from drug.tumorsimulation.cisPtCalculator import cisPtCalculatorClass

class CellularGrid:

    def __init__(self, screen, cellSize, sizex, sizey, isStandalone, deadline, coeffs,temp,capilaryList):


        self.sizex = sizex
        self.sizey = sizey
        self.standalone = isStandalone
        self.deadline = deadline
        self.cells = []
        self.cellTypeMatrix = numpy.zeros((sizex,sizey))
        self.cellStateMatrix = numpy.zeros((sizex,sizey))
        self.cellNextStateMatrix = numpy.zeros((sizex,sizey))
        self.stressScoreMatrix = numpy.zeros((sizex,sizey))
        self.cellMetabolismMatrix = numpy.ones((sizex,sizey))
        self.borderCellMatrix = numpy.zeros((sizex,sizey))
        self.screen = screen
        self.cellNumber = 0
        self.configObject = ConfigClass()
        self.mutationDisplayDict = {}
        self.mutationSubSystem = MutationSubSystemClass()
        self.temp = temp
        self.capilaryList = capilaryList

        if isStandalone == False:
            cmo,cmg,cmh,cmg_an,cmh_an,cpo,cpg,cph,cpg_an,cph_an, angio_percent = coeffs
            self.stochasticParameters = coeffs
        else:
            with sqlite3.connect('tumordb') as conn:
                cursor = conn.cursor()
            cursor.execute("""
              select Cmo,Cmg,Cmh,Cmg_an,Cmh_an,Cpo,Cpg,Cph,Cpg_an,Cph_an, angio_percent from coefficients where coeff_pk = 1
              """)
            for row in cursor.fetchall():
                cmo,cmg,cmh,cmg_an,cmh_an,cpo,cpg,cph,cpg_an,cph_an, angio_percent = row
                self.stochasticParameters = row

        for mutationName in self.mutationSubSystem.probabilityDict.keys():
            self.mutationDisplayDict[mutationName] = numpy.zeros((sizex,sizey))
        proliferationSystem = CellProliferationSubSystemClass(self.configObject,cmo,cmg,cmh,cmg_an,cmh_an,cpo,cpg,cph,cpg_an,cph_an)
        apoptosisSystem =  CellApoptosisSubSystemClass(self.configObject)
        energyMetabolismSystem = EnergyMetabolismSubSystemClass(self.configObject)
        migrationSystem = MigrationSubSystemClass(self.configObject,MigrationPhenotypeDecisionSystemClass(self.configObject),cmo,cmg,cmh,cmg_an,cmh_an)
        self.nonDimensionalizer = NonDimensionlizerClass(self.configObject)
        self.isNaturalApoptosisEnabled = True
        self.totalApoptoticCellNumber = 0
        self.apoptoticCellRatio = 0.0


        for x in xrange(0, self.sizex):
            rowcells = []
            for y in xrange(0, self.sizey):
                c = CellClass(screen, cellSize, x, y, proliferationSystem, \
                              apoptosisSystem, energyMetabolismSystem, migrationSystem, \
                              self.nonDimensionalizer, self.configObject, self.mutationSubSystem,\
                              self.cellTypeMatrix, self.cellStateMatrix, \
                              self.cellNextStateMatrix,self.stressScoreMatrix,self.borderCellMatrix)
                rowcells.append(c)
            self.cells.append(rowcells)

        from random import randint
        radiusOfTumor = 46
        centerX = int(self.sizex / 2)
        centerY = int(self.sizey / 2)
        for x in xrange(int((self.sizex / 2)-radiusOfTumor), int((self.sizex / 2)+radiusOfTumor)):
            for y in xrange(int((self.sizey / 2)-radiusOfTumor), int((self.sizey / 2)+radiusOfTumor)):
                distanceToCenter = int(math.hypot(x-centerX, y-centerY))
                chance = randint(1,100)
                
                if chance >= 20 and distanceToCenter<radiusOfTumor:
                    if distanceToCenter<(radiusOfTumor*3/4):
                        self.cells[x][y].displayState(CellClass.APOPTOSIS,0)
                        self.cellStateMatrix[x][y] = CellClass.APOPTOSIS
                        self.cellNextStateMatrix[x][y] = CellClass.APOPTOSIS
                    else:
                        self.cells[x][y].displayState(CellClass.PROLIFERATION,0)
                        self.cellStateMatrix[x][y] = CellClass.PROLIFERATION
                        self.cellNextStateMatrix[x][y] = CellClass.PROLIFERATION
                        self.cellMetabolismMatrix[x][y] = CellClass.AEROBIC
                    self.cellTypeMatrix[x][y] = CellClass.TUMOR_CELL
                    self.cellNumber += 1

        if isStandalone:
            self.createCapillary(angio_percent,None)
        else:
            self.createCapillary(None,capilaryList)

        self.o2FlowMatrix = numpy.zeros((sizex,sizey))
        self.glucoseFlowMatrix = numpy.zeros((sizex,sizey))
        self.hIyonFlowMatrix = numpy.zeros((sizex,sizey))

        self.acidProductionMatrix = numpy.zeros((sizex,sizey))
        self.acidConcentrationMatrix = numpy.ones((sizex,sizey))
        self.acidCalculator = AcidCalculatorClass(self.nonDimensionalizer,self.configObject)

        self.glucoseConsumptionMatrix = numpy.zeros((sizex,sizey))
        self.glucoseConcentrationMatrix = numpy.ones((sizex,sizey))
        self.nutritionCalculator = NutritionCalculatorClass(self.nonDimensionalizer,self.configObject)

        self.o2consumptionMatrix = numpy.zeros((sizex,sizey))
        self.o2ConcentrationMatrix = numpy.ones((sizex,sizey))
        self.o2Calculator = o2CalculatorClass(self.nonDimensionalizer,self.configObject)
        self.hypoxiaInducedApoptosisProbabilityMatrix = numpy.zeros((sizex,sizey))
        self.hypoglycemiaInducedApoptosisProbabilityMatrix = numpy.zeros((sizex,sizey))
        
        self.chemoDeathProbMatrix = numpy.zeros((sizex,sizey))
        self.migrativePhenotypeProbabilityMatrix = numpy.zeros((sizex,sizey))
        self.borderThreshold = self.configObject.proliferationBorderSearchThreshold
        self.mutationRestrictionSet = set()

        self.rc = self.nonDimensionalizer.nonDimensionalO2ConsRate
        self.rh = self.nonDimensionalizer.nonDimensionalHIyonProdRate

        if isStandalone == False:
            self.tumorGrowth = numpy.zeros(deadline+1)

        from drug.tumorsimulation.nonDimensionalizer import NonDimensionlizerClass as drugNDClass
        from drug.tumorsimulation.ConfigModule import ConfigClass as drugConfigClass

        self.cisPtInstance = cisPtCalculatorClass(drugNDClass(drugConfigClass()),drugConfigClass())

    def createCapillary(self, angio_percent,capilaryList):
        if self.standalone:
            import random
            self.cor = []
            for i in range(self.sizex*self.sizey*int(angio_percent)/100000):
                x = random.randint(0, 399)
                y = random.randint(0, 399)
                #If the cell is already a capilary then find a new location which is not a capilary cell
                while(self.cellTypeMatrix[x][y] == CellClass.CAPILARY):
                    x = random.randint(0, 399)
                    y = random.randint(0, 399)
                self.cellTypeMatrix[x][y] = CellClass.CAPILARY
                self.cor.append((x,y))
#            cor = ((50,50),(60,60),(70,70),(80,80),(90,90),(100,100),(110,110),(120,120),(130,130),(140,140),(160,160),(170,170),
#                   (230,230),(240,240),(250,250),(260,260),(270,270),(280,280),(290,290),(300,300),(310,310),(320,320),(330,330),(340,340),(350,350))
#            for x,y in cor:
#                self.cellTypeMatrix[x][y] = CellClass.CAPILARY
        else:
            for x,y in capilaryList:
                self.cellTypeMatrix[x][y] = CellClass.CAPILARY
    
    def start(self):
        #Each time step shows 22 hour
        workTime = 10000;
        cells = self.cells

        if self.configObject.timeDebug:
            startTime = time()

        if self.configObject.tumorGrowthStatistics:
            with open('./statistics/cellNumber_'+str(self.temp)+'.csv', 'w') as cellNumberLog:
                cellNumberLog.write('Time Step;Tumor Cell Number;Elapsed Time;Area (mm^2);Volume(mm^3)\n')
                cellNumberLog.write('Parameter list: ;' +str(self.stochasticParameters)+'\n')
                cellNumberLog.write('Capilary list: ;' +str(self.capilaryList) + '\n')


        if self.configObject.statisticsDebug:
            with open('./statistics/mutationStats.csv', 'w') as cellNumberLog:
                cellNumberLog.write('\n')
            with open('./statistics/deadCellNumber.csv', 'w') as cellNumberLog:
                cellNumberLog.write('Time;Dead Cell Number;Dead Cell Ratio\n')
                cellNumberLog.write('\n')

        for t in range(workTime):
            if(t<2):
                self.isCapilariesUpdated = True
            else:
                self.isCapilariesUpdated = False

            self.o2ConcentrationMatrix = numpy.array(self.calculateO2Concentration(self.o2consumptionMatrix,t,self.o2FlowMatrix))
            self.glucoseConcentrationMatrix = numpy.array(self.calculateGlucoseConcentration(self.glucoseConsumptionMatrix,t,self.glucoseFlowMatrix))
            self.acidConcentrationMatrix = numpy.array(self.calculateHIyonConcentration(self.acidProductionMatrix,t))

            # Loop through every spot in our 2D array and check spots neighbors
            # We loop all cells except edge cells
            self.cellNumberOld = self.cellNumber
            self.cellNumber = 0.0
            self.totalApoptoticCellNumberOld = self.totalApoptoticCellNumber
            self.totalApoptoticCellNumber = 0.0
            self.Xmin = 999
            self.Xmax = 0
            self.Ymin = 999
            self.Ymax = 0
            
            for x in xrange(self.sizex-2, 1, -1):
                for y in xrange(self.sizey-2, 1, -1):
#                    if self.cellTypeMatrix[x][y] == CellClass.TUMOR_CELL:
                    if self.cellStateMatrix[x][y] != 0:
                        self.cellNumber += 1
                        if x < self.Xmin:
                             self.Xmin = x
                        if x > self.Xmax:
                             self.Xmax = x
                        if y < self.Ymin:
                             self.Ymin = y
                        if y > self.Ymax:
                             self.Ymax = y

                        if self.cellStateMatrix[x][y] == CellClass.EMPTY or\
                        self.cellTypeMatrix[x][y] == CellClass.EMPTY or\
                        self.cellStateMatrix[x][y] == CellClass.APOPTOSIS:
                            if  self.cellStateMatrix[x][y] == CellClass.APOPTOSIS:
                                self.totalApoptoticCellNumber += 1
                            self.o2consumptionMatrix[x][y] = 0
                            self.glucoseConsumptionMatrix[x][y] = 0
                            self.acidProductionMatrix[x][y] = 0
                        else:
                            # We use x*y to get calculated value because o2ConcentrationMatrix is 1d but our mesh is 2d
                            targetCell1DCoordinates = (x * 400) + y
                            self.o2consumptionMatrix[x][y] =  CellMetabolism.calculateO2ConsumptionForCell(self.cellMetabolismMatrix[x][y],
                                                                                                           self.cellStateMatrix[x][y],
                                                                                                           self.rc, self.o2ConcentrationMatrix[targetCell1DCoordinates])

                            self.glucoseConsumptionMatrix[x][y] = CellMetabolism.calculateGlucoseConsumptionForCell(self.cellMetabolismMatrix[x][y], self.cellStateMatrix[x][y], \
                                                                                                                     self.nonDimensionalizer.nonDimensionalAerobicGlucoseConsRate,\
                                                                                                                     self.nonDimensionalizer.nonDimensionalAnaerobicGlucoseConsRate,
                                                                                                                     self.glucoseConcentrationMatrix[targetCell1DCoordinates])

                            self.acidProductionMatrix[x][y] = CellMetabolism.calculateAcidProductionForCell(self.cellMetabolismMatrix[x][y], self.cellStateMatrix[x][y], self.rh)

                            # Decide is total apoptotic cell ratio is greater than natural apoptosis rate
                            self.apoptoticCellRatio = (self.totalApoptoticCellNumberOld/self.cellNumberOld) * 100
                            if self.apoptoticCellRatio > self.configObject.naturalApoptosisRate:
                                self.isNaturalApoptosisEnabled = False
                            else:
                                self.isNaturalApoptosisEnabled = True
                                
                           
                            #Calculate cell death probability based on chemotherapy
                            if t > 9 and t < 21:
                                drugSchedule = 2
                                self.chemoDeathProbMatrix = self.cisPtInstance.getCellDeathProb(self.cor,self.sizex,t,drugSchedule)
                            else:
                                drugSchedule = t+1
                                self.chemoDeathProbMatrix = numpy.zeros((self.sizex,self.sizey))
#                            print(t)
                           # self.chemoDeathProbMatrix = numpy.zeros((self.sizex,self.sizey))
                            #print(self.chemoDeathProbMatrix)
                            try:
                                cells[x][y].setNextState(cells, self.o2ConcentrationMatrix, self.glucoseConcentrationMatrix,\
                                                     self.acidConcentrationMatrix, t,self.hypoxiaInducedApoptosisProbabilityMatrix,\
                                                     self.hypoglycemiaInducedApoptosisProbabilityMatrix ,self.cellMetabolismMatrix, \
                                                     self.migrativePhenotypeProbabilityMatrix,
                                                     self.mutationRestrictionSet,self.isNaturalApoptosisEnabled,
                                                     self.o2consumptionMatrix, self.glucoseConsumptionMatrix,\
                                                     self.acidProductionMatrix,self.chemoDeathProbMatrix[x][y] )
                            except:
                                print((x,y))
                                raise
                    elif self.cellTypeMatrix[x][y] == CellClass.CAPILARY and self.isCapilariesUpdated:
                        self.o2FlowMatrix[x][y] = 1.0
                        self.glucoseFlowMatrix[x][y] = 1.0
                        self.hIyonFlowMatrix[x][y] = 1.0

            # We apply stepToNextState in a seperate loop, cos' each cell should work in same time step while deciding for their behaviour
            self.mutationStatistics = {}
            for mutationName in self.mutationSubSystem.probabilityDict.keys():
                self.mutationStatistics[mutationName] = 0

            if t > 0:
                for x in xrange(self.sizex-2, 1, -1):
                    for y in xrange(self.sizex-2, 1, -1):
                        cells[x][y].stepToNextState(self.o2consumptionMatrix, self.glucoseConsumptionMatrix,\
                                                    self.acidProductionMatrix, self.migrativePhenotypeProbabilityMatrix,x,y,t)
                        for mutation in cells[x][y].dna:
                            self.mutationDisplayDict[mutation][x][y] = 1
                            self.mutationStatistics[mutation] += 1

            self.restrictOverMutations()
            self.logAndShowData(t,startTime)

            if self.standalone == False and t == self.deadline:
                return self.tumorGrowth

    def calculateO2Concentration(self, o2consumptionMatrix, timeStep, o2FlowMatrix):
        chain = itertools.chain(*o2consumptionMatrix)
        chain2 = itertools.chain(*o2FlowMatrix)
        flattenedMatrix = list(chain)
        flattenedMatrix2 = list(chain2)
        o2concentrationMatrix = self.o2Calculator.calculateO2(flattenedMatrix,
                                                              timeStep,
                                                              flattenedMatrix2,self.isCapilariesUpdated,False);
        return o2concentrationMatrix;

    def calculateGlucoseConcentration(self, glucoseConsumptionMatrix, timeStep,glucoseFlowMatrix):
        chain = itertools.chain(*glucoseConsumptionMatrix)
        chain2 = itertools.chain(*glucoseFlowMatrix)
        flattenedMatrix = list(chain)
        flattenedMatrix2 = list(chain2)
        glucoseConcentrationMatrix =\
        self.nutritionCalculator.calculateGlucose(flattenedMatrix,
                                                  timeStep,flattenedMatrix2,self.isCapilariesUpdated,False);
        return glucoseConcentrationMatrix;

    def calculateHIyonConcentration(self, acidProductionMatrix,
                                    timeStep):
        chain = itertools.chain(*acidProductionMatrix)
        flattenedMatrix = list(chain)
        acidConcentrationMatrix =\
        self.acidCalculator.calculateAcid(flattenedMatrix,
                                          timeStep,False);
        return acidConcentrationMatrix;

    def restrictOverMutations(self):
        for mutationName,mutatedCellNumber in self.mutationStatistics.items():
            mutationPercentage = mutatedCellNumber/self.cellNumber
            if mutationName not in self.mutationRestrictionSet and mutationPercentage*100 > self.mutationSubSystem.probabilityDict[mutationName][1]:
                self.mutationRestrictionSet.add(mutationName)
            elif mutationName in self.mutationRestrictionSet:
	            self.mutationRestrictionSet.remove(mutationName)

    def logAndShowData(self,t,startTime):

        #print t
        if self.configObject.showDebugGraphics:
            fig4 = figure(4)
            fig4.suptitle('Hypoxia Induced Apoptosis Probability', fontsize=10)
            imshow(self.hypoxiaInducedApoptosisProbabilityMatrix, origin='upper')
            if self.configObject.save_images and t%self.configObject.imageSaveInterval == 0:
                nameofpic = "./images/hypoxiaInducedApoptosisProbability_" + str(t) + ".png"
                savefig(nameofpic)

            fig5 = figure(5)
            fig5.suptitle('O2 Consumption', fontsize=10)
            imshow(self.o2consumptionMatrix, origin='upper')
            if self.configObject.save_images and t%self.configObject.imageSaveInterval == 0:
                nameofpic = "./images/o2consumption_" + str(t) + ".png"
                savefig(nameofpic)

            fig6 = figure(6)
            fig6.suptitle('Energy Metabolism', fontsize=10)
            imshow(self.cellMetabolismMatrix, origin='upper')
            if self.configObject.save_images and t%self.configObject.imageSaveInterval == 0:
                nameofpic = "./images/energyMetabolism_" + str(t) + ".png"
                savefig(nameofpic)

            fig7 = figure(7)
            fig7.suptitle('Hypoxia Induced Apoptosis Probability', fontsize=10)
            imshow(self.hypoglycemiaInducedApoptosisProbabilityMatrix, origin='upper')
            if self.configObject.save_images and t%self.configObject.imageSaveInterval == 0:
                nameofpic = "./images/hypoglycemiaInducedApoptosisProbabilityMatrix_" + str(t) + ".png"
                savefig(nameofpic)

            fig8 = figure(8)
            fig8.suptitle('Migrative Cells', fontsize=10)
            imshow(self.migrativePhenotypeProbabilityMatrix, origin='upper')
            if self.configObject.save_images and t%self.configObject.imageSaveInterval == 0:
                nameofpic = "./images/migrativePhenotypeProbabilityMatrix" + str(t) + ".png"
                savefig(nameofpic)

            fig8 = figure(9)
            fig8.suptitle('Migrative Cells', fontsize=10)
            imshow(self.cellTypeMatrix, origin='upper')
            if self.configObject.save_images and t%self.configObject.imageSaveInterval == 0:
                nameofpic = "./images/cellTypeMatrix" + str(t) + ".png"
                savefig(nameofpic)

            mutationFiguresCount = 9
            for mutationName, mutationMatrix in self.mutationDisplayDict.items():
                mutationFiguresCount +=1
                figureTmp = figure(mutationFiguresCount)
                figureTmp.suptitle(mutationName +' Mutation', fontsize=10)
                imshow(mutationMatrix, origin='upper')
                if self.configObject.save_images and t%self.configObject.imageSaveInterval == 0:
                    nameofpic = "./images/" + mutationName + "Mutation_" + str(t) + ".png"
                    savefig(nameofpic)

            if self.configObject.disableXServer == False:
                show()

        if self.configObject.save_images and t%self.configObject.imageSaveInterval == 0:
            pygame.display.update()
            nameofpic = "./images/tumor_" + str(t) + ".png"
            pygame.image.save(self.screen, nameofpic)

        if t == 17 and self.configObject.timeDebug:
            elapsed = time() - startTime
            print("Finished. Total time for 17 step:" + strftime('%H:%M:%S',
                                                                 gmtime(elapsed)))

        if t == 34 and self.configObject.timeDebug:
            elapsed = time() - startTime
            print("Finished. Total time for 34 steps: " + strftime('%H:%M:%S',
                                                                   gmtime(elapsed)))

        if self.configObject.tumorGrowthStatistics:
            with open('./statistics/cellNumber_'+str(self.temp)+'.csv', 'a') as cellNumberLog:
                elapsed = time() - startTime
                # Cancer cell density is 1.6 * 10^5 cell cm^-2
                # %10 is calculated for spaces in tumor
                #area = 9 * self.cellNumber *1.1*self.configObject.cellRepresentationCoeff/1600
                #volume= math.pow(math.sqrt(area),3)
                
                yLength = self.Ymax - self.Ymin
                xLength = self.Xmax - self.Xmin

                # each cell is 0.025 mm but corner of each cell represents 9
                # cells virtually and one corner has 3 cells virtually
                lengthTumor = max(xLength,yLength) * 0.025 * 3
                widthTumor = min(xLength,yLength) * 0.025 * 3
                # ellipse
                area = 3.14 * lengthTumor * widthTumor
                # Calculated according to "Role of hypoxia and glycolysis in the
                # development of multi-drug resistance in human tumor cells and
                # the establishment of an orthotopic multi-drug resistant tumor
                # model in nude mice using hypoxic pre-conditioning" paper
                volume = (lengthTumor * (widthTumor**2))/2
                cellNumberLog.write(str(t) +';'+str(9*self.cellNumber)+';'+strftime('%H:%M:%S', gmtime(elapsed))+';' + str(area)+';'+str(volume)+'\n')
                if self.standalone == False:
                    self.tumorGrowth[t] = volume

        if self.configObject.statisticsDebug:
            with open('./statistics/deadCellNumber.csv', 'a') as cellNumberLog:
                cellNumberLog.write(str(t) +';'+str(self.totalApoptoticCellNumber)+';'+str(self.apoptoticCellRatio)+'\n')
            with open('./statistics/mutationStats.csv', 'a') as cellNumberLog:
                col = 1
                for mutationName,mutatedCellNumber in self.mutationStatistics.items():
                    if t == 0:
                        cellNumberLog.write(';' + mutationName)
                    elif col == 1:
                        cellNumberLog.write(str(t) + ';'+str(mutatedCellNumber))
                    else:
                        cellNumberLog.write(';'+str(mutatedCellNumber))
                    col += 1
                    cellNumberLog.write('\n')
