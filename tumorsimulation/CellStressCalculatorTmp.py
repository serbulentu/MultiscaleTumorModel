__author__ = 'srb'

import CellUtils

def calculateCellStress(cells,  x,  y, borderThreshold, stressScoreMatrix, borderCellMatrix):

    if borderCellMatrix[x][y] == 1:
        pass

    borderCellList = CellUtils.getBorderCells( x,  y, borderThreshold,borderCellMatrix)

    if len(borderCellList) > 0:
        stressScoreMatrix[x][y] = CellUtils.findMinLenghtToBorder(borderCellList, x, y) + 1.0
    else:
        stressScoreMatrix[x][y] = 99

