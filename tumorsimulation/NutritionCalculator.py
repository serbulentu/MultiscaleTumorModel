__author__ = 'Serbulent Unsal'

from fipy import *
import numpy as np

class NutritionCalculatorClass:

    def __init__(self,nonDimensionaliser,configObject):

        self.conf = configObject

        nx = configObject.nx
        cellSize = configObject.cellSize
        nx = nx/cellSize
        dx = configObject.dx_real
        ny = nx
        dy = dx
        mesh = Grid2D(dx=dx, dy=dy, nx=nx, ny=ny)

        self.phi = CellVariable(name="Glucose Concentration", mesh=mesh, value = 1.0 )

        self.nonDimensionalizer = nonDimensionaliser
        D = self.nonDimensionalizer.nonDimensionalGlucoseDiffConst

        self.phi.constrain(0.0, mesh.facesLeft)
        self.phi.constrain(0.0, mesh.facesRight)
        self.phi.constrain(0.0, mesh.facesTop)
        self.phi.constrain(0.0, mesh.facesBottom)

        proliferationAge = configObject.paStandart
        self.timeStepDuration = 1/proliferationAge

        self.viewer = Viewer(vars=self.phi)

        self.source = CellVariable(mesh=mesh)
        self.large_value = CellVariable(mesh=mesh)
        self.sourceCapilary = CellVariable(mesh=mesh)
        self.eq = TransientTerm() == DiffusionTerm(coeff=D) - self.source - ImplicitSourceTerm(self.large_value) + self.sourceCapilary * self.large_value
        #self.eq = TransientTerm() == DiffusionTerm(coeff=D)  - self.source

        #import fipy.solvers.trilinos as solvers
        #self.solver = solvers.linearPCGSolver.LinearPCGSolver(precon=None, iterations=40, tolerance=1e-15)

    def calculateGlucose(self, sourceTerm,timeStep, sourceCapilary,
                         isCapilariesUpdated, isTest):
    #setvalue() has a notable computational cost so we check if anything changes
        if(isCapilariesUpdated):
            bigValue = 10**10
            val = np.multiply(sourceCapilary,bigValue)
            self.large_value.setValue(val)
            self.sourceCapilary.setValue(sourceCapilary)

        self.source.setValue(sourceTerm)
        self.eq.solve(var=self.phi, dt=self.timeStepDuration)
        willImagesSaved = self.conf.save_images
        if isTest is False and willImagesSaved and timeStep%self.conf.imageSaveInterval == 0:
            nameofpic = "./images/glucoseDiffusion_" + str(timeStep) + ".png"
            self.viewer.plot(filename=nameofpic)
        else:
            self.viewer.plot()

        #TSVViewer(vars=(phi)).plot(filename="output.txt")

        # globalValue
        # Concatenate and return values from all processors
        # When running on a single processor, the result is identical to value.

        return self.phi.globalValue;
