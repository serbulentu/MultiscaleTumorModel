__author__ = 'Serbulent Unsal'

from fipy import *
import numpy as np

class o2CalculatorClass:

    def __init__(self,nonDimensionaliser,configObject):

        self.conf = configObject

        nx = configObject.nx
        cellSize = configObject.cellSize
        nx = nx/cellSize
        dx = configObject.dx_real
        ny = nx
        dy = dx

        mesh = Grid2D(dx=dx, dy=dy, nx=nx, ny=ny)

        boundaryCondition = 0.0
        self.phi = CellVariable(name="Oxygen Concentration", mesh=mesh, value = 1.0 )

        self.nonDimensionalizer = nonDimensionaliser
        D = self.nonDimensionalizer.nonDimensionalO2DiffConst

        self.phi.constrain(boundaryCondition, mesh.facesLeft)
        self.phi.constrain(boundaryCondition, mesh.facesRight)
        self.phi.constrain(boundaryCondition, mesh.facesTop)
        self.phi.constrain(boundaryCondition, mesh.facesBottom)

        proliferationAge = configObject.paStandart
        self.timeStepDuration = 1/proliferationAge

        self.viewer = Viewer(vars=self.phi)

        self.source = CellVariable(mesh=mesh)
        self.large_value = CellVariable(mesh=mesh)
        self.sourceCapilary = CellVariable(mesh=mesh)
        self.eq = TransientTerm() == DiffusionTerm(coeff=D) - self.source - ImplicitSourceTerm(self.large_value) + self.sourceCapilary * self.large_value

    def calculateO2(self, sourceTerm, timeStep,
                    sourceCapilary,isCapilariesUpdated,isTest):
    #setvalue() has a notable computational cost so we check if anything changes
        if(isCapilariesUpdated):
            bigValue = 10**10
            val = np.multiply(sourceCapilary,bigValue)
            self.large_value.setValue(val)
            self.sourceCapilary.setValue(sourceCapilary)

        self.source.setValue(sourceTerm)
        self.eq.solve(var=self.phi, dt=self.timeStepDuration)
        willImagesSaved = self.conf.save_images
        if isTest is False and willImagesSaved and timeStep%self.conf.imageSaveInterval == 0:
            nameofpic = "./images/o2Diffusion_" + str(timeStep) + ".png"
            self.viewer.plot(filename=nameofpic)
        else:
            self.viewer.plot()
        #TSVViewer(vars=(phi)).plot(filename="output.txt")

        # globalValue
        # Concatenate and return values from all processors
        # When running on a single processor, the result is identical to value.
        return self.phi.globalValue;


