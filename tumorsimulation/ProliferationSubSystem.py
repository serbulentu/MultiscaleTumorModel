__author__ = 'Serbulent Unsal'

import Cell
import CellUtils
import tumorsimulation.ProliferationProbabiltyCalculator as ProliferationProbabiltyCalculator
import tumorsimulation.InvasionSubSystem as InvasionSubSystem
from CellShifter import CellShifterClass

PROLIFERATION = 1
MIGRATION = 2


class CellProliferationSubSystemClass():

    def __init__(self, configObject, cmo, cmg, cmh, cmg_an, cmh_an, cpo, cpg, cph, cpg_an, cph_an):
        self.configObject = configObject
        self.cmo = cmo
        self.cmg = cmg
        self.cmh = cmh
        self.cmg_an = cmg_an
        self.cmh_an = cmh_an
        self.cpo = cpo
        self.cpg = cpg
        self.cph = cph
        self.cpg_an = cpg_an
        self.cph_an = cph_an

        csc = CellShifterClass(cmo, cmg, cmh, cmg_an, cmh_an)
        self.shiftCellsStartFrom = csc.shiftCellsStartFrom

    # Detect if cell will produce or not and produce if microenviroment and
    # cell is suitable
    def willCellProliferate(self, cells, x, y, cellMetabolismMatrix, o2ConcentrationMatrix,
                            glucoseConcentrationMatrix, acidConcentrationMatrix,
                            candidateCellState, cellStateMatrix, cellNextStateMatrix, cellTypeMatrix,
                            cellStressScoreMatrix,
                            borderCellMatrix, timeStep):
        candidateCell = cells[x][y]
        candidateCellCoordinates1D = x + (400 * y)

        if candidateCellState == Cell.CellClass.QUIESCENCE or candidateCellState == Cell.CellClass.PROLIFERATION:
            willCellProliferationOccur = ProliferationProbabiltyCalculator.willCellProliferationOccurs(
                cellMetabolismMatrix[x][y],
                o2ConcentrationMatrix[candidateCellCoordinates1D],
                glucoseConcentrationMatrix[candidateCellCoordinates1D],
                acidConcentrationMatrix[
                    candidateCellCoordinates1D], candidateCell.proliferationGeneticEffect,
                self.configObject.o2Background, self.configObject.hypoxiaInducedApoptosisTreshold,
                self.configObject.glucoseBackground, self.configObject.apoptosisTresholdForGlucose,
                self.configObject.optimalPh, self.configObject.phBackground,
                self.configObject.minPh, self.configObject.maxPh,
                self.cpo, self.cpg, self.cph, self.cpg_an, self.cph_an, self.configObject.coefficentForMiliMolartoMol)
            if willCellProliferationOccur:
                cellStateMatrix[x][y] = Cell.CellClass.PROLIFERATION
                cellNextStateMatrix[x][y] = Cell.CellClass.PROLIFERATION
                coordinatesOfInvasion = InvasionSubSystem.findMostSuitableNeighbourToInvade(cellStressScoreMatrix,
                                                                    cellTypeMatrix,
                                                                    cellMetabolismMatrix[x][y],
                                                                    x, y, o2ConcentrationMatrix,
                                                                    glucoseConcentrationMatrix,
                                                                    acidConcentrationMatrix,
                                                                    self.configObject.phBackground,
                                                                    self.configObject.cellStressScoreThreshold,
                                                                    timeStep, 1,
                                                                    self.cmo, self.cmg,
                                                                    self.cmh, self.cmg_an,
                                                                    self.cmh_an,
                                                                    self.configObject.coefficentForMiliMolartoMol, 0)

                if coordinatesOfInvasion == 0:
                    return False

                if cellTypeMatrix[coordinatesOfInvasion[0]][coordinatesOfInvasion[1]] == Cell.CellClass.EMPTY:
                    CellUtils.createNewCell(cells, cellStateMatrix, candidateCell, cellNextStateMatrix, cellTypeMatrix,
                                            coordinatesOfInvasion[0], coordinatesOfInvasion[1],cellMetabolismMatrix)
#                elif cellTypeMatrix[coordinatesOfInvasion[0]][coordinatesOfInvasion[1]] == Cell.CellClass.TUMOR_CELL:
#                    processedCellList = []
#
#                    self.shiftCellsStartFrom(cells,cellStateMatrix,  cellStressScoreMatrix,
#                                             cells[coordinatesOfInvasion[0]][
#                                                 coordinatesOfInvasion[1]],
#                                             cellMetabolismMatrix, processedCellList,
#                                             o2ConcentrationMatrix, glucoseConcentrationMatrix,
#                                             acidConcentrationMatrix, cellNextStateMatrix, cellTypeMatrix,
#                                             borderCellMatrix, timeStep, self.configObject, 1)
#
#                return True
            else:
                return False
            #            except Exception as inst:
            #               print inst.message
            #              print 'cellTypeMatrix: ' + str(cellTypeMatrix)
            # print 'coordinatesOfInvasion: ' + str(coordinatesOfInvasion)
