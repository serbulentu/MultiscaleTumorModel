__author__ = 'Serbulent Unsal'

from Cell import CellClass
from random import randint

class EnergyMetabolismSubSystemClass():

    def __init__(self,configObject):
        self.configObject = configObject
        self.aerobicCellNumber = 0
        self.anaerobicCellNumber = 0
        self.oldTimeStep = 0
        if self.configObject.statisticsDebug:
            with open('./statistics/energyMetabolismCellNumber.csv', 'w') as cellNumberLog:
                cellNumberLog.write('Time;Aerobic Cell Number;Anerobic Cell Number\n')

    def decideMetabolismOfCell(self,targetCell, cellMetabolismMatrix, o2Concentration, timeStep):
        if self.configObject.statisticsDebug:
            if timeStep > self.oldTimeStep:
                self.oldTimeStep = timeStep
                if self.configObject.statisticsDebug:
                    with open('./statistics/energyMetabolismCellNumber.csv', 'a') as cellNumberLog:
                        cellNumberLog.write(str(timeStep) +';'+ str(self.aerobicCellNumber) +';'+ str(self.anaerobicCellNumber) + '\n')
                self.aerobicCellNumber = 0
                self.anaerobicCellNumber = 0

        if (o2Concentration < self.configObject.hypoxiaInducedGlycolysisTresholdMax):
            if o2Concentration < self.configObject.hypoxiaInducedGlycolysisTresholdMin:
                targetCell.hypoxiaReceptorDelayCounter += 1
            else:
                #Probability function is 1/x hypoxiaInducedGlycolysisTresholdMin is 0.01 equals %100 and  hypoxiaInducedGlycolysisTresholdMax 0.05 equals %20
                o2Conc =  o2Concentration / self.configObject.o2Background
                probability = 1/o2Conc
                chanceForMetabolismShift = randint(1,100)
                if probability > chanceForMetabolismShift:
                    targetCell.hypoxiaReceptorDelayCounter += 1

            # In real world there will be a random delay between o2 conc drops down under hypoxiaInducedGlycolysisTreshold level
            # and hypoxia induced metabolsim shift so we try to simulate this effect below
            chanceForMetabolismShift = randint(1,self.configObject.hypoxiaReceptorDelay)
            if targetCell.hypoxiaReceptorDelayCounter > chanceForMetabolismShift:
                cellMetabolismMatrix[targetCell.cellPositionX][targetCell.cellPositionY] = CellClass.ANAEROBIC
                if self.configObject.statisticsDebug:
                    self.anaerobicCellNumber +=1
            else:
                self.changeMetabolismToAEROBIC(targetCell,cellMetabolismMatrix, targetCell.hypoxiaReceptorDelayCounter)
        else:
            self.changeMetabolismToAEROBIC(targetCell, cellMetabolismMatrix,  0)

    def changeMetabolismToAEROBIC(self, targetCell, cellMetabolismMatrix, hypoxiaReceptorDelayCounter):
        cellMetabolismMatrix[targetCell.cellPositionX][targetCell.cellPositionY] = CellClass.AEROBIC
        targetCell.hypoxiaReceptorDelayCounter = hypoxiaReceptorDelayCounter
        if self.configObject.statisticsDebug:
            self.aerobicCellNumber += 1

