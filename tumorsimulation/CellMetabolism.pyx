__author__ = 'Serbulent Unsal'

import cython
from Cell import CellClass

cdef int AEROBIC = CellClass.AEROBIC
cdef int ANAEROBIC = CellClass.ANAEROBIC
cdef int QUIESCENCE = CellClass.QUIESCENCE
cdef int PROLIFERATION = CellClass.PROLIFERATION
cdef int MIGRATIVE = CellClass.MIGRATIVE

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
def calculateO2ConsumptionForCell(int energyMetabolism, int state, double rc, double o2Concentration):
    cdef int energyMetabolismCoeff = 1
 
    if energyMetabolism == AEROBIC:
        energyMetabolismCoeff = 1
    elif energyMetabolism == ANAEROBIC:
        energyMetabolismCoeff = 0


    return energyMetabolismCoeff * rc #* 1.5 * 9;

"""   if state == PROLIFERATION:
#        if (o2Concentration > 0):
        return energyMetabolismCoeff * rc #* 1.5 * 9;
#        else:
#            return 0;
    elif state == QUIESCENCE:
#        if (o2Concentration > 0):
        return energyMetabolismCoeff * rc #* 9;
#        else:
#            return 0;
    elif state == MIGRATIVE:
#        if (o2Concentration > 0):
        return energyMetabolismCoeff * rc #* 1.5 * 9;
#        else:
#            return 0;
#    else:
#        return 0;
"""

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
def calculateGlucoseConsumptionForCell(int energyMetabolism, int state,
                                       double nonDimensionalAerobicGlucoseConsRate,
                                       double nonDimensionalAnaerobicGlucoseConsRate,
                                       glucoseConcentration):
    cdef double rg = 0.0

    if energyMetabolism == AEROBIC:
        rg = nonDimensionalAerobicGlucoseConsRate
        #print 'rg = nonDimensionalAnaerobicGlucoseConsRate ... rg = ' + str(rg)
    elif energyMetabolism == ANAEROBIC:
        rg = nonDimensionalAnaerobicGlucoseConsRate
        #print 'rg = nonDimensionalAnaerobicGlucoseConsRate ... rg = ' + str(rg)

    return rg;
    
#    if state == PROLIFERATION:
#        cons = rg * 1.5 * 9;
#        if glucoseConcentration > cons:
#            return rg * 1.5 * 9;
#        else:
#            return 0
#    elif state == QUIESCENCE:
#        cons = rg * 9;
#        if glucoseConcentration > cons:
#            return rg * 9;
#        else:
#            return 0
#    elif state == MIGRATIVE:
#        cons = rg * 1.5 * 9;
#        if glucoseConcentration > cons:
#            return rg * 1.5 * 9;
#        else:
#            return 0
#    else:
#        return 0.0;

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
def calculateAcidProductionForCell(int energyMetabolism, int state, double rh):
    cdef int energyMetabolismCoeff = 0

    if energyMetabolism == AEROBIC:
        energyMetabolismCoeff = 0
    elif energyMetabolism == ANAEROBIC:
        energyMetabolismCoeff = 1

    return energyMetabolismCoeff * rh;

#    if state == PROLIFERATION:
#        return energyMetabolismCoeff * rh * 1.5 * 9;
#    elif state == QUIESCENCE:
#        return energyMetabolismCoeff * rh * 9;
#    elif state == MIGRATIVE:
#        return energyMetabolismCoeff * rh * 1.5 * 9;
#    else:
#        return 0;
