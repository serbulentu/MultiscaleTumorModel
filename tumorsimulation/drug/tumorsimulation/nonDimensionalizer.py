__author__ = 'Serbulent Unsal'


class NonDimensionlizerClass:

    nonDimensionalO2DiffConst = 0
    nonDimensionalO2ConsRate = 0

    nonDimensionalGlucoseDiffConst = 0
    nonDimensionalAerobicGlucoseConsRate = 0
    nonDimensionalAneerobicGlucoseConsRate = 0

    nonDimensionalHIyonDiffConst = 0
    nonDimensionalHIyonProdRate = 0

    def __init__(self, configObject):

        o2BackgroundConcentration = configObject.o2Background
        glucoseBackgroundConcentration = configObject.glucoseBackground

        proliferationTime = configObject.proliferationTime

        nx = configObject.nx
        cellSize = configObject.cellSize
        real_nx = nx/cellSize
        dx = configObject.dx
        real_dx = configObject.dx_real
        real_dy = real_dx
        L = 3 #dx * real_nx
        area = L*L
        areaReal = 1
        cellNumber = area/(real_dx*real_dy)
        #print cellNumber

        o2DiffConst = configObject.o2DiffConst
        cisPtDiffConst = configObject.cisPtDiffConst

        o2ConsRate = configObject.o2ConsRate

        glucoseDiffConst = configObject.glucoseDiffConst
        glucoseAerobicConsRate = configObject.glucoseAerobicConsRate
        glucoseAnaerobicConsRate = configObject.glucoseAnaerobicConsRate

        acidDiffConst = configObject.hydrogenIyonDiffConst
        acidProdRate = configObject.acidProdRate

        self.nonDimensionalO2DiffConst = self.nonDimensionlizeDiffusionConstant(o2DiffConst,proliferationTime, area)
        self.nonDimensionalcisPtDiffConst = self.nonDimensionlizeDiffusionConstant(cisPtDiffConst, proliferationTime, area)
        self.nonDimensionalO2ConsRate = self.nonDimensionlizeConsumptionOrProductionRate(proliferationTime,cellNumber,o2ConsRate,o2BackgroundConcentration)

        #print self.nonDimensionalcisPtDiffConst

        self.nonDimensionalGlucoseDiffConst =  self.nonDimensionlizeDiffusionConstant(glucoseDiffConst,proliferationTime,area)
        self.nonDimensionalAerobicGlucoseConsRate = self.nonDimensionlizeConsumptionOrProductionRate(proliferationTime,cellNumber,glucoseAerobicConsRate,glucoseBackgroundConcentration)
        self.nonDimensionalAnaerobicGlucoseConsRate = self.nonDimensionlizeConsumptionOrProductionRate(proliferationTime,cellNumber,glucoseAnaerobicConsRate,glucoseBackgroundConcentration)


    def nonDimensionlizeDiffusionConstant(self,DiffusionConstant,time,area):
        nonDimensionalDiffusionConstant = (DiffusionConstant*time*3600)/area
        return nonDimensionalDiffusionConstant

    def nonDimensionlizeConsumptionOrProductionRate(self,time,cellNumber,rate,backGroundConcentration):
        nonDimensionalRate = (time* 3600 * cellNumber * rate)/backGroundConcentration
        return nonDimensionalRate

