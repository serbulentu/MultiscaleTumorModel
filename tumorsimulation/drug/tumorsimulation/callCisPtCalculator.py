__author__ = 'Nihat Burak Zihni'

from tumorsimulation.cisPtCalculator import cisPtCalculatorClass
from tumorsimulation.ConfigModule import ConfigClass
from tumorsimulation.nonDimensionalizer import NonDimensionlizerClass

import numpy

configObject = ConfigClass()
nonDimensionalizer = NonDimensionlizerClass(configObject)

# Capilary list is something like;

cor = ((50,50),(60,60),(70,70),(80,80),(90,90),(100,100),(110,110),(120,120),(130,130),(140,140),(160,160),(170,170),
(230,230),(240,240),(250,250),(260,260),(270,270),(280,280),(290,290),(300,300),(310,310),(320,320),(330,330),(340,340), (350,350))

timeStep=5
drugSchedule=8 # larger than timeStep means drug administered only once
sizex = 400
sizey = 400



cisPtCalcInstance = cisPtCalculatorClass(nonDimensionalizer, configObject)  # get an instance of the class
veri=cisPtCalcInstance.getCellDeathProb(cor,sizex,timeStep,drugSchedule)
print veri[190 * 400 + 190]