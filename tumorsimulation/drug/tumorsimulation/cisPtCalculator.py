__author__ = 'Nihat Burak Zihni'

from fipy import *
import numpy as np
import itertools
import os


class cisPtCalculatorClass:
    def __init__(self, nonDimensionaliser, configObject):

        self.conf = configObject
        self.imagePath = os.path.dirname(os.path.dirname(__file__))
        self.indis = 0
        self.cisPt = configObject.nonDimenCisPtAmount

        self.nx = configObject.nx
        cellSize = configObject.cellSize
        self.nx = self.nx / cellSize
        dx = configObject.dx_real
        self.ny = self.nx
        dy = dx

        self.xycor = list()
        self.cisPtconsM = np.full((self.nx, self.ny), self.conf.drugDecayRate * 2 / 3)
        self.cisPtFlowMatrix = np.zeros((self.nx, self.ny))

        mesh = Grid2D(dx=dx, dy=dy, nx=self.nx, ny=self.ny)

        boundaryCondition = 0.0
        self.phi = CellVariable(name="Cisplatin Concentration", mesh=mesh, value=0.0)
        self.phi2 = CellVariable(name="Cell Death Probability", mesh=mesh, value=0.0)
        self.phi3 = CellVariable(name="Drug Decay Calculation", mesh=mesh, value=0.0)

        self.nonDimensionalizer = nonDimensionaliser
        D = self.nonDimensionalizer.nonDimensionalcisPtDiffConst

        self.phi.constrain(boundaryCondition, mesh.facesLeft)
        self.phi.constrain(boundaryCondition, mesh.facesRight)
        self.phi.constrain(boundaryCondition, mesh.facesTop)
        self.phi.constrain(boundaryCondition, mesh.facesBottom)

        proliferationAge = configObject.paStandart
        self.timeStepDuration = 1 / proliferationAge

        self.viewer = Viewer(vars=self.phi, datamin=0., datamax=1.)
        self.viewer2 = Viewer(vars=self.phi2, datamin=0., datamax=100.)

        self.sourceDecay = CellVariable(mesh=mesh)
        self.large_value = CellVariable(mesh=mesh)
        self.large_value2 = CellVariable(mesh=mesh)
        self.sourceCapilary = CellVariable(mesh=mesh)
        self.eq = TransientTerm() == DiffusionTerm(coeff=D) - ImplicitSourceTerm(
            self.large_value) + self.sourceCapilary * self.large_value - self.sourceDecay * self.phi

    def getCellDeathProb(self, capilaryList, nx, timeStep, drugSchedule):
        print("cell Death Prob is Calculating")
        print capilaryList

        # -------------begin of for decay initializiation for capilary and elsewhere in the domain
        for x in range(nx):
            for y in range(nx):
                key = (x, y)
                self.xycor.append(key)

        for x, y in capilaryList:
            key = (x, y)
            try:
                self.xycor.remove(key)
            except ValueError:
                pass  # do nothing!

                # -------------end of for decay initializiation for capilary and elsewhere in the domain

        for x, y in capilaryList:
            self.cisPtFlowMatrix[x][y] = 1.0
            self.cisPtconsM[x][y] = self.conf.drugDecayRate

        consumptionChain = itertools.chain(*self.cisPtconsM)
        flowChain = itertools.chain(*self.cisPtFlowMatrix)
        cFM = list(consumptionChain)
        fFM = list(flowChain)
        self.cisPtConcentrationMatrix = []
        self.cellDeathProbMatrix = []
        self.calculateDecayMatrix = []

        for t in range(timeStep):
            if (timeStep % drugSchedule) != 0:
                for x, y in capilaryList:
                    self.cisPtFlowMatrix[x][y] = self.cisPtFlowMatrix[x][y] - (
                        self.cisPtFlowMatrix[x][y] * self.cisPtconsM[x][y])
                flowChain = itertools.chain(*self.cisPtFlowMatrix)
                fFM = list(flowChain)
            elif (timeStep % drugSchedule) == 0:
                for x, y in capilaryList:
                    self.cisPtFlowMatrix[x][y] = 1.0
                flowChain = itertools.chain(*self.cisPtFlowMatrix)
                fFM = list(flowChain)

            self.cisPtConcentrationMatrix = self.calculateCisPt(cFM, fFM, True, True)
            print self.cisPtConcentrationMatrix
            self.calculateDecayMatrix = self.calculateDrugDecayOutsideCapilary(
                self.cisPtConcentrationMatrix, t, self.cisPtconsM, nx, True, True)
            print self.calculateDecayMatrix
            self.cisPtConcentrationMatrix = self.calculateDecayMatrix
            amountFlattenedMatrix = list(self.cisPtConcentrationMatrix)
            print 'Cell Death probability test calculating time step: ' + str(t)
            self.cellDeathProbMatrix = self.calculateCellDeathProb(
                amountFlattenedMatrix, t, True, True)
            print self.cellDeathProbMatrix

        return np.reshape(self.cellDeathProbMatrix, (nx, nx))

    def calculateCisPt(self, sourceTerm, sourceCapilary, isCapilariesUpdated, isTest):
        # setvalue() has a notable computational cost so we check if anything changes
        if (isCapilariesUpdated):
            bigValue = 10 ** 10
            val = np.multiply(sourceCapilary, bigValue)
            self.large_value.setValue(val)
            self.sourceCapilary.setValue(sourceCapilary)
            self.sourceDecay.setValue(sourceTerm)

        self.eq.solve(var=self.phi, dt=self.timeStepDuration)
        # self.phi[self.phi < 0] = 0.0
        return self.phi.globalValue;

    def calculateDrugDecayOutsideCapilary(self, amountTermWithoutOutDecay, timeStep,
                                          decayRateOutCapilaries, dimenX, isCapilariesUpdated, isTest):
        # setvalue() has a notable computational cost so we check if anything changes
        if (isCapilariesUpdated):
            for x, y in self.xycor:
                amountTermWithoutOutDecay[x * dimenX + y] = amountTermWithoutOutDecay[x * dimenX + y] - np.multiply(
                    amountTermWithoutOutDecay[x * dimenX + y], decayRateOutCapilaries[x][y])
        amountDecayMatrix = list(amountTermWithoutOutDecay)
        self.phi3.setValue(amountDecayMatrix)
        # self.phi[self.phi3 < 0] = 0.0
        self.phi.setValue(self.phi3)

        willImagesSaved = self.conf.save_images
        # if isTest is False and willImagesSaved and timeStep % self.conf.imageSaveInterval == 0:
        #     nameofpic = str(self.imagePath) + "/images/cisDiffusion_" + str(timeStep) + ".png"
        #     self.viewer.plot(filename=nameofpic)
        # else:
        #     nameofpic = str(self.imagePath) + "/images/cisDiffusion_" + str(timeStep) + ".png"
        #     self.viewer.plot(filename=nameofpic)

        return self.phi3.globalValue;

    def calculateCellDeathProb(self, amountTerm, timeStep,
                               isCapilariesUpdated, isTest):
        # setvalue() has a notable computational cost so we check if anything changes
        if (isCapilariesUpdated):
            val3 = np.multiply(amountTerm, self.cisPt)
            val2 = np.square(val3) * 792258 - np.multiply(val3, 641) + 13
            print(self.cisPt)
            self.phi2.setValue(val2)

        # self.phi2[self.phi2 < 0] = 0.0

        willImagesSaved = self.conf.save_images
        # if isTest is False and willImagesSaved and timeStep % self.conf.imageSaveInterval == 0:
        #     nameofpic2 = str(self.imagePath) + "/images/cellDeathProb_" + str(timeStep) + ".png"
        #     self.viewer2.plot(filename=nameofpic2)
        # else:
        #     nameofpic2 = str(self.imagePath) + "/images/cellDeathProb_" + str(timeStep) + ".png"
        #     self.viewer2.plot(filename=nameofpic2)

        return self.phi2.globalValue;
