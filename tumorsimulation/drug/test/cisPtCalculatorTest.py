from __future__ import absolute_import
"""CisPtcalculator Test Module"""

__author__ = 'Nihat Burak Zihni'


# pylint:disable=import-error,too-many-public-methods,invalid-name,attribute-defined-outside-init,wrong-import-position,wrong-import-order

import matplotlib as mpl
mpl.use('Agg')
import unittest
import itertools
import math
import numpy
import os
import random as rnd
from fipy import *
from tumorsimulation.cisPtCalculator import cisPtCalculatorClass
from tumorsimulation.ConfigModule import ConfigClass
from tumorsimulation.nonDimensionalizer import NonDimensionlizerClass


class TestSequenceFunctions(unittest.TestCase):
    """
    cisPtCalculator Test Class
    This class test cisPt diffusion equation at external and internal boundaries
    """

    def setUp(self):
        """init before test"""
        self.cellCount = open("testfile.txt", "w")
        self.tumorCount = open("tumorfile.txt", "w")

        configObject = ConfigClass()
        nonDimensionalizer = NonDimensionlizerClass(configObject)
        self.cisPtCalculator = cisPtCalculatorClass(nonDimensionalizer, configObject)

        self.sizex = 400
        self.sizey = 400
        self.kStep=4
        self.ilac=8
        self.sayac=1

        self.cisPtconsumptionMatrix = numpy.full((self.sizex, self.sizey),configObject.drugDecayRate*2/3)
        self.cisPtFlowMatrix = numpy.zeros((self.sizex, self.sizey))

        # create capilary matrix
        self.cor = list()
        with open("capfile.txt", "rb") as fp:
            for i in fp.readlines():
                tmp = i.split(",")
                try:
                    x=int(tmp[0])
                    y=int(tmp[1])
                    key = (x, y)
                    self.cor.append(key)
                except:
                    pass

        #-------------for decay initializiation for capilary and elsewhere in the domain
        self.xycor = list()

        for x in range(self.sizex):
            for y in range(self.sizey):
                key = (x, y)
                self.xycor.append(key)

        for x, y in self.cor:
            key = (x, y)
            try:
                self.xycor.remove(key)
            except ValueError:
                pass  # do nothing!

        # -------------end of for decay initializiation for capilary and elsewhere in the domain

        for x, y in self.cor:
            self.cisPtFlowMatrix[x][y] = 1.0
            self.cisPtconsumptionMatrix[x][y] = configObject.drugDecayRate
    def test_calculateCisPtConcentration(self):
        """
        test cisPt diffusion equation at external and internal boundaries
        """

        consumptionChain = itertools.chain(*self.cisPtconsumptionMatrix)
        flowChain = itertools.chain(*self.cisPtFlowMatrix)
        consumptionFlattenedMatrix = list(consumptionChain)
        flowFlattenedMatrix = list(flowChain)
        self.cisPtConcentrationMatrix = []
        self.cellDeathProbMatrix = []
        self.calculateDecayMatrix = []

        for t in range(5):
            if (t%self.ilac)!=0:
                for x, y in self.cor:
                    self.cisPtFlowMatrix[x][y] = self.cisPtFlowMatrix[x][y] - (
                    self.cisPtFlowMatrix[x][y] * self.cisPtconsumptionMatrix[x][y])
                flowChain = itertools.chain(*self.cisPtFlowMatrix)
                flowFlattenedMatrix = list(flowChain)
            elif (t%self.ilac) == 0:
                for x, y in self.cor:
                    self.cisPtFlowMatrix[x][y] = 1.0
                flowChain = itertools.chain(*self.cisPtFlowMatrix)
                flowFlattenedMatrix = list(flowChain)
            print 'Cisplatin diffusion test calculating time step: ' + str(t)

            self.cisPtConcentrationMatrix = self.cisPtCalculator.calculateCisPt(
            consumptionFlattenedMatrix, flowFlattenedMatrix, True, True)
            # else:

            self.calculateDecayMatrix = self.cisPtCalculator.calculateDrugDecayOutsideCapilary(
                self.cisPtConcentrationMatrix, t, self.cisPtconsumptionMatrix,self.sizex, True, True)
            self.cisPtConcentrationMatrix = self.calculateDecayMatrix
            amountFlattenedMatrix = list(self.cisPtConcentrationMatrix)
            # print self.o2ConcentrationMatrix[50 * 400 + 50]
            # print self.o2ConcentrationMatrix[10 * 400 + 10]
            print 'Cell Death probability test calculating time step: ' + str(t)
            self.cellDeathProbMatrix = self.cisPtCalculator.calculateCellDeathProb(
                amountFlattenedMatrix, t, True, True)
            self.tumorCount.write(str(len([1 for i in self.cellDeathProbMatrix if i > 60])*9)+os.linesep)

        # Check values at borders they should be 0
        borders = ((0, 0), (self.sizex - 1, 0), (0, self.sizey - 1),
                   (self.sizex - 1, self.sizey - 1))
        for x, y in borders:
            targetCell1DCoordinates = (x * 400) + y
            # This will only printed if test fails
            print 'Coordinates: ' + str((x, y)) + 'Cisplatin Concentration is: ' \
                + str(self.cisPtConcentrationMatrix[targetCell1DCoordinates])
            self.assertTrue(numerix.allclose(self.cisPtConcentrationMatrix[targetCell1DCoordinates],
                                             0.0, atol=1e-2))
            print 'Coordinates: ' + str((x, y)) + 'Cell Death probability is: ' \
                  + str(self.cellDeathProbMatrix[targetCell1DCoordinates])
            self.assertTrue(numerix.allclose(self.cellDeathProbMatrix[targetCell1DCoordinates],
                                             0.0, atol=1e-2))
        self.cellCount.close()

if __name__ == '__main__':
    unittest.main()