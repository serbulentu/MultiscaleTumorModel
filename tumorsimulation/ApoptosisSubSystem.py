__author__ = 'Serbulent Unsal'

import tumorsimulation.PhCalculatorCython as PhCalculatorCython

from Cell import CellClass
from random import randint

class CellApoptosisSubSystemClass():

    def __init__(self,configObject):
        self.configObject = configObject
        self.apoptoticCellNumber = 0
        if self.configObject.statisticsDebug:
            with open('./statistics/apoptoticCellNumber.csv', 'w') as cellNumberLog:
                cellNumberLog.write('Time;Apoptotic Cell Number\n')
        self.oldTimeStep = 0

    def willTherapyTriggersApoptosis(self,targetCell, targetCellState,
                                     cellNextStateMatrix, timeStep):

        x = targetCell.cellPositionX
        y = targetCell.cellPositionY

        if self.configObject.statisticsDebug:
            if timeStep > self.oldTimeStep:
                self.oldTimeStep = timeStep
                with open('./statistics/apoptoticCellNumber.csv', 'a') as cellNumberLog:
                    cellNumberLog.write(str(timeStep) +';'+ str(self.apoptoticCellNumber) + '\n')
                self.apoptoticCellNumber = 0

        targetCell1DCoordinates = (x * 400) + y
        
        therapyDay = timeStep - 20
        fname = "/home/servertbb/Code/MSTM/multiscaletumormodel/tumorsimulation/therapy/prob_"+ str(therapyDay) + ".txt"
        probList = [line.rstrip('\n') for line in open(fname)]

        therapyInducedDeathProbability =  int(float(probList[targetCell1DCoordinates]))
        survivalPossibility = randint(1,100)
        if therapyInducedDeathProbability  > survivalPossibility:
            return self.startApoptosisForTherapy(targetCell,cellNextStateMatrix,x,y)
        else:
            return False


    def willCellGoesToApoptosis(self,targetCell, targetCellState, cellNextStateMatrix, o2Concentration, glycoseConcentration,\
                                hIyonConcentration, timeStep, hypoxiaInducedApoptosisProbabilityMatrix,\
                                hypoglycemiaInducedApoptosisProbabilityMatrix,cellMetabolismMatrix,
                                isNaturalApoptosisEnabled,chemoDeathProb):

        x = targetCell.cellPositionX
        y = targetCell.cellPositionY

        if self.configObject.statisticsDebug:
            if timeStep > self.oldTimeStep:
                self.oldTimeStep = timeStep
                with open('./statistics/apoptoticCellNumber.csv', 'a') as cellNumberLog:
                    cellNumberLog.write(str(timeStep) +';'+ str(self.apoptoticCellNumber) + '\n')
                self.apoptoticCellNumber = 0

        if timeStep > -1:
            if self.apoptosisStarted(targetCellState):
                return True

            # lets check for natural apoptosis survivalPossibility at first
            if isNaturalApoptosisEnabled:
                survivalPossibility = randint(1,100)
                if self.configObject.naturalApoptosisRate > survivalPossibility:
                    return self.startApoptosis(targetCell,cellNextStateMatrix,x,y)

            #check for hypoxia induced apoptosis between %1.5 and %0 Oxygen cencentration apoptosis survivalPossibility
            # varies between %10 percent and %55 percent, so for each %0.1 we add apoptosis survivalPossibility %3
            # i.e if oxygen level is %1 we calculate probability as ((1.5 -1)/0.1)*3 + 10 = 25
            # Apoptosis probabilty is %25 for that cell
            #time interval ekle

            if  cellMetabolismMatrix[x][y] == CellClass.AEROBIC and o2Concentration < self.configObject.hypoxiaInducedApoptosisTreshold:
                o2Percentage = o2Concentration/self.configObject.o2Background
                hypoxiaInducedApoptosisProbability = (((1.5 - o2Percentage)/0.1)*3)+10
                hypoxiaInducedApoptosisProbability -= targetCell.apoptosisEffect

                #print "hypoxiaInducedApoptosisProbability: " +  str(hypoxiaInducedApoptosisProbability)
                hypoxiaInducedApoptosisProbabilityMatrix[targetCell.cellPositionX][targetCell.cellPositionY] = hypoxiaInducedApoptosisProbability

                survivalPossibility = randint(1,100)
                if hypoxiaInducedApoptosisProbability > survivalPossibility:
                    return self.startApoptosis(targetCell,cellNextStateMatrix,x,y)
                # if o2Concentration = 0 and cell will not die it should choose anerobic metabolism
                elif o2Concentration <= 0:
                    cellMetabolismMatrix[x][y] = CellClass.ANAEROBIC
                    return False
            # check hypoglycemia induced apoptosis between hypoglycemia treshold and zero
            # we increase apoptosis survivalPossibility %1 for each portion
            if glycoseConcentration < self.configObject.apoptosisTresholdForGlucose:
                if glycoseConcentration <= 0:
                    return self.startApoptosis(targetCell,cellNextStateMatrix,x,y)
                else:
                    onePercent = self.configObject.apoptosisTresholdForGlucose/100
                    hypoglycemiaInducedApoptosisProbability = glycoseConcentration/onePercent
                    hypoglycemiaInducedApoptosisProbability -= targetCell.apoptosisEffect

                    hypoglycemiaInducedApoptosisProbabilityMatrix[targetCell.cellPositionX][targetCell.cellPositionY] = hypoglycemiaInducedApoptosisProbability

                    survivalPossibility = randint(1,100)
                    if hypoglycemiaInducedApoptosisProbability > survivalPossibility:
                        return self.startApoptosis(targetCell,cellNextStateMatrix,x,y)

            cellMicroenviromentPh = PhCalculatorCython.convertHIyonConcentrationToPH(hIyonConcentration,self.configObject.coefficentForMiliMolartoMol)
            if cellMicroenviromentPh <= self.configObject.phApoptosisTreshold:
                onePercent = (self.configObject.phApoptosisTreshold -self.configObject.minPh)/100
                acidInducedApoptosisProbability = (cellMicroenviromentPh - self.configObject.minPh)/onePercent
                acidInducedApoptosisProbability -= targetCell.apoptosisEffect
                survivalPossibility = randint(1,100)
                if acidInducedApoptosisProbability > survivalPossibility:
                    return self.startApoptosis(targetCell,cellNextStateMatrix,x,y)
            elif PhCalculatorCython.convertHIyonConcentrationToPH(hIyonConcentration,self.configObject.coefficentForMiliMolartoMol) >= self.configObject.maxPh:
                return self.startApoptosis(targetCell,cellNextStateMatrix,x,y)

            chemoSurvivalPossibility = randint(1,100)
            if chemoDeathProb > chemoSurvivalPossibility:
                return self.startApoptosisForTherapy(targetCell,cellNextStateMatrix,x,y)

    def startApoptosisForTherapy(self, targetCell, cellNextStateMatrix, x, y):
        cellNextStateMatrix[x][y] = CellClass.APOPTOSIS
        self.apoptoticCellNumber += 1
        return True

    def startApoptosis(self, targetCell, cellNextStateMatrix, x, y):
        targetCell.apoptosisDelayCounter += 1
        chanceForApoptosis = randint(1,self.configObject.apoptosisDelay)
        if targetCell.apoptosisDelayCounter > chanceForApoptosis:
            cellNextStateMatrix[x][y] = CellClass.APOPTOSIS
            self.apoptoticCellNumber += 1
            return True

    def apoptosisStarted(self,targetCellState):
        if targetCellState == CellClass.APOPTOSIS:
            return True
        else:
            return False
