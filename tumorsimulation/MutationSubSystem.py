__author__ = 'Serbulent Unsal'

import numpy
from random import randint

class MutationSubSystemClass():

    def __init__(self):

        self.ancestorDict = {'EGFR' : None,
                          'KRAS' : None,
                          'NTRK3' : None ,
                          'TP53' : ['KRAS'],
                          'ATM' : ['KRAS'],
                          'STK11' : ['TP53','NTRK3'] ,
                          'CDK2NA' : ['TP53'] ,
                          }

        self.probabilityDict = {'EGFR' : (10,35),
                                'KRAS' : (33,33),
                                'NTRK3' : (3,3) ,
                                'TP53' : (30,50),
                                'ATM' : (10,20),
                                'STK11' : (18,18) ,
                                'CDK2NA' : (5,5) ,
                                }
        # Cancer Genome Landscapes,Vogelstein et. al. , 2013
        #each driver mutation provides only a small selective growth advantage to the cell, on the order of a 0.4% increase in the difference between cell birth and cell death.
        #But since only %5-%10 percent of these are driver mutations we
        # calculate each driver will effect 8 percent
        self.metabolismEffectsDict = {'EGFR' : {'proliferation':8},
                             'KRAS' : {'proliferation':8},
                             'NTRK3' : {'proliferation':8} ,
                             'TP53' : {'apoptosis':8},
                             'ATM' : {'proliferation':8},
                             'STK11' : {'apoptosis':8} ,
                             'CDK2NA' : {'apoptosis':8} ,
                             }



    '''Function returns mutation probability.
    If probality is in a range it produce a random number in this range and returns it.'''
    def getMutationProbability(self,mutation):
        pList = self.probabilityDict[mutation]
        return randint(pList[0],pList[1])

    '''Function check preconditions of mutation bases on ancestors'''
    def ancestorsExists(self,mutation, targetCell):
        ancetorList = self.ancestorDict[mutation]
        for ancestor in ancetorList:
            if ancestor in targetCell.dna:
                return True
            else:
                return False

    '''Function check preconditions of mutation bases on biological rules'''
    def arePreConditionsSatisfiedForMutation(self,mutation,cellDNA):
        # Rule 'EGFR mutations never occurred in tumors with KRAS mutations'
        if (mutation == 'EGFR' and 'KRAS' in cellDNA) or (mutation == 'KRAS' and 'EGFR' in cellDNA):
            return False
        else:
            return True

    ''' Function triggers mutation process'''
    def mutate(self,targetCell, mutationRestrictionSet):
        for mutation in self.ancestorDict:
            if mutation not in targetCell.dna \
                and self.arePreConditionsSatisfiedForMutation(mutation, targetCell.dna) \
            and mutation not in mutationRestrictionSet:
                self.tryMutation(mutation,targetCell)

        self.applyGeneticEffectsToCell(targetCell)

    '''Function determine mutation probability and if probability greater than chance add it to cell.
    If mutation has ancestors it checks them first'''
    def tryMutation(self,mutation, targetCell):
        if self.ancestorDict[mutation] is None:
            self.useChanceForMutation(mutation, targetCell)
        elif self.ancestorsExists(mutation, targetCell):
            self.useChanceForMutation(mutation, targetCell)

    ''' Mutation process occurs in this function'''
    def useChanceForMutation(self, mutation, targetCell):
        mutationProbality = self.getMutationProbability(mutation)
        chance = randint(1,10000)/100.0
        if (mutationProbality/100.0) >= chance:
            targetCell.dna.append(mutation)

    ''' In the function genetic effects applied to cell'''
    def applyGeneticEffectsToCell(self,targetCell):
        proliferationEffect = 0
        apoptosisEffect = 0
        for mutation in targetCell.dna:
            effectDict = self.metabolismEffectsDict[mutation]
            for metabolism,effect in effectDict.items():
                if metabolism == 'proliferation':
                    proliferationEffect += effect
                elif metabolism == 'apoptosis':
                    apoptosisEffect += effect
        targetCell.proliferationEffect = proliferationEffect
        targetCell.apoptosisEffect = apoptosisEffect



