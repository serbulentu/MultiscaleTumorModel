from __future__ import print_function

__author__ = 'Serbulent Unsal'

import tumorsimulation.PhCalculatorCython
import pygame, time, numpy
import CellStressCalculatorTmp


class CellClass():
    cellPositionX = 0
    cellPositionY = 0

    """Define state of cells"""
    EMPTY = 0
    NECROSIS = 1
    APOPTOSIS = 2
    QUIESCENCE = 3
    PROLIFERATION = 4
    MIGRATIVE = 5

    #Define Cell Type
    EMPTY = 0
    TUMOR_CELL = 1
    CAPILARY = 2

    #Define Metabolism Type
    AEROBIC = 50
    ANAEROBIC = 100

    screen = None


    def __init__(self, screen, cellSize, cellPositionX, cellPositionY, \
                 proliferationSystem, apoptosisSystem, energyMetabolismSystem, \
                 migrationSystem, nonDimensionalizer, configObject, mutationSystem,\
                 cellTypeMatrix, cellStateMatrix, cellNextStateMatrix, cellStressScoreMatrix , borderCellMatrix):

        self.cellTypeMatrix = cellTypeMatrix
        self.cellStateMatrix = cellStateMatrix
        self.cellNextStateMatrix = cellNextStateMatrix
        self.cellStressScoreMatrix = cellStressScoreMatrix
        self.borderCellMatrix = borderCellMatrix

#        self.cellStateMatrix[self.cellPositionX][self.cellPositionY] = CellClass.QUIESCENCE
        self.hypoxiaReceptorDelayCounter = 0
        self.migrationDelayCounter = 0
        self.apoptosisDelayCounter = 0
        self.metabolismCoeff = 1
        self.dna = []
        self.proliferationGeneticEffect = 0
        self.apoptosisEffect = 0
        #Age of cell
        self.age = 0
        self.apoptosisAge = 0
        self.optimalAcidConcentration = 0
        self.cellSize = 0

        self.cellSize = cellSize
        self.screen = screen
        self.cellPositionX = cellPositionX
        self.cellPositionY = cellPositionY
        self.proliferationSystem = proliferationSystem
        self.mutationSystem = mutationSystem
        self.nonDimensionalizer = nonDimensionalizer
        self.apoptosisSystem = apoptosisSystem
        self.energyMetabolismSystem = energyMetabolismSystem
        self.migrationSystem = migrationSystem

        self.ProliferationAge = configObject.paStandart
        self.optimalAcidConcentration = configObject.optimalPh

        self.borderThreshold = configObject.proliferationBorderSearchThreshold

        self.configObject = configObject
        self.willImagesSaved = configObject.save_images
        self.numberOfEmptyNeighbours = 0


    def setNextState(self, cells, o2ConcentrationMatrix, glucoseConcentrationMatrix, \
                     acidConcentrationMatrix, timeStep, hypoxiaInducedApoptosisProbabilityMatrix,
                     hypoglycemiaInducedApoptosisProbabilityMatrix, cellMetabolismMatrix,
                     migrativePhenotypeProbabilityMatrix, mutationRestrictionSet,
                     isNaturalApoptosisEnabled,o2consumptionMatrix, glucoseConsumptionMatrix, \
                     acidProductionMatrix, chemoDeathProb):
        cells[self.cellPositionX][self.cellPositionY].age += 1


        # In every 5 th step a mutation could happen
        if timeStep != 0 and timeStep % self.configObject.mutationDelay == 0:
            self.mutationSystem.mutate(cells[self.cellPositionX][self.cellPositionY], mutationRestrictionSet)

        #Find how many neighbours in a 3x3 surrounding grid
        self.numberOfEmptyNeighbours = 0;
        for i in range(-1, 2):
            for j in range(-1, 2):
                if (i != 0 or j != 0) and self.cellTypeMatrix[self.cellPositionX + i][self.cellPositionY + j] == CellClass.EMPTY:
                    self.numberOfEmptyNeighbours += 1

        targetCell1DCoordinates = (self.cellPositionX * 400) + self.cellPositionY
        o2Concentration = float(o2ConcentrationMatrix[targetCell1DCoordinates]) * self.configObject.o2Background
        glucoseConcentration = float(glucoseConcentrationMatrix[targetCell1DCoordinates]) * self.configObject.glucoseBackground
        hIyonConcentration = (1 + float(acidConcentrationMatrix[
            targetCell1DCoordinates])) * self.configObject.HIyonBackGround

        self.energyMetabolismSystem.decideMetabolismOfCell(cells[self.cellPositionX][self.cellPositionY], cellMetabolismMatrix,
                                                           o2Concentration, timeStep)

        try:
            len(self.cellNextStateMatrix)
        except:
            return

        self.borderControl()
        CellStressCalculatorTmp.calculateCellStress(cells,self.cellPositionX,self.cellPositionY, self.borderThreshold,self.cellStressScoreMatrix,self.borderCellMatrix)
        
        if timeStep > 20 and self.apoptosisSystem.willTherapyTriggersApoptosis(cells[self.cellPositionX][self.cellPositionY],\
                                                          self.cellStateMatrix[self.cellPositionX][self.cellPositionY],\
                                                          self.cellNextStateMatrix, timeStep):
            return
        elif self.migrationSystem.willCellMigrate(cells, self.cellStateMatrix, self.cellStressScoreMatrix,
                    cells[self.cellPositionX][self.cellPositionY],self.cellTypeMatrix, self.cellNextStateMatrix,\
                    cellMetabolismMatrix, o2Concentration, glucoseConcentration, \
                    hIyonConcentration,o2ConcentrationMatrix, glucoseConcentrationMatrix,acidConcentrationMatrix, \
                    migrativePhenotypeProbabilityMatrix,
                                                  self.borderCellMatrix,
                                                  timeStep, o2consumptionMatrix, glucoseConsumptionMatrix, \
                    acidProductionMatrix):
                
                return
        elif self.cellStressScoreMatrix[self.cellPositionX][self.cellPositionY] <  self.configObject.cellStressScoreThreshold\
            and self.age > self.ProliferationAge :

            self.proliferationSystem.willCellProliferate(cells, self.cellPositionX, \
                                                         self.cellPositionY, cellMetabolismMatrix, o2ConcentrationMatrix, \
                                                         glucoseConcentrationMatrix, acidConcentrationMatrix,
                                                         self.cellStateMatrix[self.cellPositionX][self.cellPositionY],\
                                                         self.cellStateMatrix, self.cellNextStateMatrix, self.cellTypeMatrix, self.cellStressScoreMatrix,
                                                         self.borderCellMatrix, timeStep)

            return
        elif self.apoptosisSystem.willCellGoesToApoptosis(cells[self.cellPositionX][self.cellPositionY],\
                                                          self.cellStateMatrix[self.cellPositionX][self.cellPositionY],\
                                                          self.cellNextStateMatrix,\
                                                          o2Concentration, \
                                                          glucoseConcentration, hIyonConcentration, timeStep,
                                                          hypoxiaInducedApoptosisProbabilityMatrix,
                                                          hypoglycemiaInducedApoptosisProbabilityMatrix,
                                                          cellMetabolismMatrix,
                                                          isNaturalApoptosisEnabled,
                                                          chemoDeathProb):

            return
        elif self.cellStressScoreMatrix[self.cellPositionX][self.cellPositionY] >= self.configObject.cellStressScoreThreshold \
            or o2Concentration < self.configObject.oxygenTresholdProliferativeToQuiscence \
            or glucoseConcentration < self.configObject.glucoseTresholdProliferativeToQuiscence:
            self.cellNextStateMatrix[self.cellPositionX][self.cellPositionY] = CellClass.QUIESCENCE
            return

    def stepToNextState(self, o2consumptionMatrix, glucoseConsumptionMatrix, \
                        acidProductionMatrix, migrativePhenotypeProbabilityMatrix, x, y,timeStep):

        if self.cellStateMatrix[self.cellPositionX][self.cellPositionY] == CellClass.APOPTOSIS or self.cellTypeMatrix[self.cellPositionX][self.cellPositionY] == CellClass.EMPTY:
            self.setParametersToZero(o2consumptionMatrix, glucoseConsumptionMatrix, \
                                     acidProductionMatrix, migrativePhenotypeProbabilityMatrix,
                                     x, y)
        self.displayState(self.cellNextStateMatrix[self.cellPositionX][self.cellPositionY],timeStep)

    def setParametersToZero(self, o2consumptionMatrix, glucoseConsumptionMatrix, \
                            acidProductionMatrix, \
                            migrativePhenotypeProbabilityMatrix, x, y):
        o2consumptionMatrix[x][y] = 0
        glucoseConsumptionMatrix[x][y] = 0
        acidProductionMatrix[x][y] = 0
        #energyMetabolismMatrix[x][y] = 0
        migrativePhenotypeProbabilityMatrix[x][y] = 0
        self.cellStressScoreMatrix[self.cellPositionX][self.cellPositionY] = 0


    def borderControl(self):
        if self.numberOfEmptyNeighbours > 4:
            self.borderCellMatrix[self.cellPositionX][self.cellPositionY] = 1
        else:
            self.borderCellMatrix[self.cellPositionX][self.cellPositionY] = 0

    def displayState(self, newstate,timeStep):

        self.cellStateMatrix[self.cellPositionX][self.cellPositionY] = newstate

        if self.configObject.save_images and timeStep%self.configObject.imageSaveInterval == 0:
            x = self.cellPositionX * self.cellSize
            y = self.cellPositionY * self.cellSize
            width = self.cellSize
            height = self.cellSize
            coordinates = (x, y, width, height )
            red = (250, 0, 0)
            green = (0, 250, 0)
            blue = (0, 0, 250)
            black = (0, 0, 0)
            white = (250,250,250)

#            if self.borderCellMatrix[self.cellPositionX][self.cellPositionY]:
#                pygame.draw.rect(self.screen, (250,250,250), coordinates)

            if self.cellTypeMatrix[self.cellPositionX][self.cellPositionY] == CellClass.EMPTY:
                pygame.draw.rect(self.screen, black, coordinates)
            elif self.cellStateMatrix[self.cellPositionX][self.cellPositionY] == CellClass.PROLIFERATION:
                pygame.draw.rect(self.screen, red, coordinates)
            elif self.cellStateMatrix[self.cellPositionX][self.cellPositionY] == CellClass.QUIESCENCE:
                pygame.draw.rect(self.screen, green, coordinates)
            elif self.cellStateMatrix[self.cellPositionX][self.cellPositionY] == CellClass.MIGRATIVE:
                pygame.draw.rect(self.screen, white, coordinates)
            elif self.cellStateMatrix[self.cellPositionX][self.cellPositionY] == CellClass.APOPTOSIS:
                pygame.draw.rect(self.screen, blue, coordinates)

    def logToFile(self, logString):
        f = open('log.txt', 'w')
        f.write(logString + '\n')
        f.close()
