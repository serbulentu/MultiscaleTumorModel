__author__ = 'Serbulent Unsal'

import tumorsimulation.InvasionSubSystem as InvasionSubSystem
import CellUtils
import Cell


PROLIFERATION = 1
MIGRATION = 2


class CellShifterClass():

    def __init__(self, cmo, cmg, cmh, cmg_an, cmh_an):
        self.depth = 0
        self.cmo = cmo
        self.cmg = cmg
        self.cmh = cmh
        self.cmg_an = cmg_an
        self.cmh_an = cmh_an

    '''
        This function detects weakest neighbour and shift cells till it finds a space.
        In this case only one more shift needed to fill space by shifting outer cell.
    '''

    def shiftCellsStartFrom(self, cells,cellStateMatrix, cellStressScoreMatrix, invaderCell, cellMetabolismMatrix, processedCellList,
                            o2ConcentrationMatrix, glucoseConcentrationMatrix,
                            acidConcentrationMatrix, cellNextStateMatrix, cellTypeMatrix,
                            borderCellMatrix, timeStep, configObject, invasionType):
        if len(processedCellList) == 0:
            self.depth = 0
        else:
            self.depth = self.depth + 1
            # print 'depth ' + str(self.depth)
        maxSearchDepth = configObject.proliferationBorderSearchThreshold
        borderCellList = CellUtils.getBorderCells(invaderCell.cellPositionX, invaderCell.cellPositionY, maxSearchDepth,
                                                  borderCellMatrix)
        if len(borderCellList) <= 0:
            # print 'no border cell'
            return

        if type(cellMetabolismMatrix) is int:
            print "cellMetabolismMatrix is an int"

        (weakestNeighbourCellX, weakestNeighbourCellY) = \
            InvasionSubSystem.findMostSuitableNeighbourToInvade(cellStressScoreMatrix,
                                                                cellTypeMatrix,
                                                                cellMetabolismMatrix[
                                                                    invaderCell.cellPositionX][
                                                                    invaderCell.cellPositionY],
                                                                invaderCell.cellPositionX,
                                                                invaderCell.cellPositionY,
                                                                o2ConcentrationMatrix,
                                                                glucoseConcentrationMatrix,
                                                                acidConcentrationMatrix,
                                                                configObject.phBackground,
                                                                configObject.cellStressScoreThreshold,
                                                                timeStep,
                                                                invasionType,
                                                                self.cmo,
                                                                self.cmg,
                                                                self.cmh,
                                                                self.cmg_an,
                                                                self.cmh_an,
                                                                configObject.coefficentForMiliMolartoMol, 0)

        weakestNeighbourCell = cells[
            weakestNeighbourCellX][weakestNeighbourCellY]
        weakestNeighbourCellType = cellTypeMatrix[
            weakestNeighbourCellX][weakestNeighbourCellY]

        if weakestNeighbourCellType == Cell.CellClass.EMPTY:
            CellUtils.createNewCell(cells,cellStateMatrix, invaderCell, cellNextStateMatrix, cellTypeMatrix, weakestNeighbourCellX,
                                    weakestNeighbourCellY,cellMetabolismMatrix)
            # print 'empty cell invaded'
            cellTypeMatrix[invaderCell.cellPositionX][
                invaderCell.cellPositionY] = Cell.CellClass.EMPTY
            return
        else:
            self.borderControl(cellTypeMatrix, weakestNeighbourCell)
            if weakestNeighbourCell.borderCell:
                x = invaderCell.cellPositionX
                y = invaderCell.cellPositionY
                coordinatesOfInvasion = InvasionSubSystem.findMostSuitableNeighbourToInvade(
                    cellStressScoreMatrix,
                    cellTypeMatrix,
                    cellMetabolismMatrix[x][y],
                    x, y, o2ConcentrationMatrix,
                    glucoseConcentrationMatrix,
                    acidConcentrationMatrix,
                    configObject.phBackground,
                    configObject.cellStressScoreThreshold,
                    timeStep, 1,
                    self.cmo, self.cmg,
                    self.cmh, self.cmg_an,
                    self.cmh_an,
                    configObject.coefficentForMiliMolartoMol, 0)

                # print 'invasion difference: ' +
                # str((weakestNeighbourCell.cellPositionX -
                # coordinatesOfInvasion[0],  weakestNeighbourCell.cellPositionY
                # - coordinatesOfInvasion[1]))
                
                try:
                    if cellTypeMatrix[coordinatesOfInvasion[0]][coordinatesOfInvasion[1]] == Cell.CellClass.EMPTY:
                        isCellCreated = CellUtils.createNewCell(cells, cellStateMatrix, weakestNeighbourCell, cellNextStateMatrix, cellTypeMatrix,
                                                coordinatesOfInvasion[0], coordinatesOfInvasion[1],cellMetabolismMatrix)
                        if isCellCreated:
                            cellTypeMatrix[weakestNeighbourCell.cellPositionX][weakestNeighbourCell.cellPositionY] = Cell.CellClass.EMPTY
                        return
                    else:
                        # print 'Error in Shift'
                        return
                except:
                    print "coordinatesOfInvasion: " +  str(coordinatesOfInvasion)
                    print "cellTypeMatrix[coordinatesOfInvasion[0]][coordinatesOfInvasion[1]]:  " +  str(cellTypeMatrix[coordinatesOfInvasion[0]][coordinatesOfInvasion[1]])
            else:
                if self.depth < maxSearchDepth - 1:
                    newInvader = weakestNeighbourCell
                    processedCellList.append(
                        (invaderCell.cellPositionX, invaderCell.cellPositionY))
                    CellUtils.createNewCell(cells,cellStateMatrix, invaderCell, cellNextStateMatrix, cellTypeMatrix,
                                            weakestNeighbourCellX,
                                            weakestNeighbourCellY, cellMetabolismMatrix)
                    cellTypeMatrix[invaderCell.cellPositionX][
                        invaderCell.cellPositionY] = Cell.CellClass.EMPTY
                    # print 'Shift diff: ' + str( (invaderCell.cellPositionX -
                    # weakestNeighbourCell.cellPositionX,
                    # invaderCell.cellPositionY -
                    # weakestNeighbourCell.cellPositionY ))
                    '''
                    This is recursive part of the code. In this part cells will push each other till border of tumor. Since we check that there is at least one border cell in search radius
                    recursive algorithm have to find a shifting route from candidate cell to border cell
                    '''
                    self.shiftCellsStartFrom(cells, cellStateMatrix,  cellStressScoreMatrix, newInvader, cellMetabolismMatrix,
                                             processedCellList,
                                             o2ConcentrationMatrix, glucoseConcentrationMatrix,
                                             acidConcentrationMatrix, cellNextStateMatrix, cellTypeMatrix,
                                             borderCellMatrix, timeStep, configObject, invasionType)
                else:
                    # print 'No empty Canditates found until max search depth'
                    return

    def borderControl(self, cellTypeMatrix, targetCell):
        self.numberOfEmptyNeighbours = 0

        for i in range(-1, 2):
            for j in range(-1, 2):
                # TODO this shouldn't be here
                if targetCell.cellPositionX + i < 400 and targetCell.cellPositionY + j < 400:
                    if (i != 0 or j != 0) and cellTypeMatrix[targetCell.cellPositionX + i][
                            targetCell.cellPositionY + j] == Cell.CellClass.EMPTY:
                        self.numberOfEmptyNeighbours += 1
                        # print 'numberOfEmptyNeighbours: ' +
                        # str(self.numberOfEmptyNeighbours)
        if self.numberOfEmptyNeighbours > 3:
            targetCell.borderCell = True
        else:
            targetCell.borderCell = False
