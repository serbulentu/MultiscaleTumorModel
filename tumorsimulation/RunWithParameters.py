from SimulationMain import Simulator

simulator = Simulator()

def runSimulation(coeffs,temp, capilaryList):
	deadline = 34
	tumorGrowth = simulator.main(False, deadline, coeffs,temp,capilaryList)
	return tumorGrowth

#coeffs =  [0.1, 0.1, 0.8, 0.2, 0.8, 0.2, 0.2, 0.6, 0.2, 0.7, 16.0]

#capilaryList = [(107, 65), (219, 327), (89, 182), (158, 57), (99, 147), (111, 349), (399, 26), (152, 399), (142, 155), (7, 216), (136, 315), (90, 83), (202, 284), (186, 309), (168, 258), (355, 378), (299,63), (312, 364), (118, 355), (337, 132), (212, 371), (183, 108), (96, 61), (56, 372), (151, 143), (61, 101), (233, 158), (71, 254), (161, 116), (304, 352), (388, 42), (49, 227)]

coeffs = [0.1, 0.4, 0.4, 0.7, 0.2, 0.6, 0.3, 0.1, 0.8, 0.1, 41.0]

capilaryList = [(334, 68), (2, 246), (223, 297), (374, 363), (7, 321), (366,295),
                (337, 144), (197, 346), (130, 312), (233, 83), (318, 229), (224, 178),
                (254, 39), (72, 359), (71, 8), (316, 399), (378, 95), (144, 80),
                (123, 133), (120, 157), (23, 330), (15, 61), (252,181),
                (324, 41), (283, 3), (26, 0), (63, 223), (393, 38), (292, 214),
                (282, 399), (342, 364), (383, 234), (369, 118), (371, 44),
                (289, 373), (242, 226), (336, 150), (148, 353), (144, 159),
                (26, 279), (70, 194), (360, 285), (155, 177), (105, 320), (307,107),
                (378, 118), (107, 250), (302, 270), (230, 325), (239, 126),
                (64, 198)]

runSimulation(coeffs,'runWithParametersTest',capilaryList)
