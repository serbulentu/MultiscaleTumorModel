__author__ = 'srb'

from distutils.core import setup
from Cython.Build import cythonize

setup(name = 'NSCLC Tumor Model',
      ext_modules = cythonize("*.pyx"))