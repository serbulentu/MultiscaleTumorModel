__author__ = 'Serbulent Unsal'

import math

class ConfigClass():

    # __slots__ is most efficient way to access attributes
    # http://stackoverflow.com/questions/1336791/dictionary-vs-object-which-is-more-efficient-and-why

    __slots__ = ('disableXServer','save_images','imageSaveInterval','showDebugGraphics','timeDebug','statisticsDebug','tumorGrowthStatistics','nx',\
                 'cellSize','cellRepresentationCoeff','dx_real','dx','L','coefficentForMiliMolartoMol','coefficentForO2mmHgToMol','paStandart','proliferationTime','naturalApoptosisRate',\
                 'o2Background','glucoseBackground','phBackground','o2DiffConst','o2ConsRate','hypoxiaInducedApoptosisTreshold','hypoxiaInducedGlycolysisTresholdMax',\
                 'hypoxiaInducedGlycolysisTresholdMin','hypoxiaReceptorDelay','hypoxiaInducedApoptosisRate','oxygenTresholdProliferativeToQuiscence','apoptosisTresholdForGlucose','glucoseDiffConst'\
                  ,'glucoseAerobicConsRate','glucoseAnaerobicConsRate','glucoseTresholdProliferativeToQuiscence','phApoptosisTreshold','minPh','maxPh','optimalPh','hydrogenIyonDiffConst',\
                 'acidProdRate','mutationDelay', 'migrationDelay','apoptosisDelay','hypoxiaInducedMigrationTreshold','hypoglicemiaInducedMigrationTreshold','phInducedMigrationTreshold',\
                 'proliferationBorderSearchThreshold','cellStressScoreThreshold')

    #[simulation_config]
    disableXServer = True
    save_images = True
    imageSaveInterval = 1
    showDebugGraphics = False
    timeDebug = True
    statisticsDebug = True
    tumorGrowthStatistics = True

    #[mesh_config]
    nx = 800
    cellSize = 2
    # Each grid cell represent 9 biological cells
    # Do we really need this ?  cos' we make L = 3 already ?
    cellRepresentationCoeff = 1
    # size of one egde of a tumor cell
    dx_real = 0.0025
    # Since each cell assumed as square to calculate dx (lenght of one cell's edge) we use sqrt method
    dx = dx_real * math.sqrt(cellRepresentationCoeff)
    #L1=3
    L = 3 # dx * (nx/cellSize)

    # Our volume is L*L*dx and 1000 milimol is 1 mol and  1000cm^3 is 1 liter
    # Since miliMolar means 10^-3 mol/l  and 1cm^3 equals 10 cm^2 so we calculate our coeff as;
    coefficentForMiliMolartoMol = (L*L*dx) * 10**-3 * 10 **-3 #* 10# mol/cm^2 *

    # We convert p02 mmHg to miliMolar and miliMolar to mol/cm^2
    # to convert mmHg to miliMolar we use a = 1.39*10**-3 as solubility coefficient of oxygen in plasma
    coefficentForO2mmHgToMol = 1.39*10**-3 * coefficentForMiliMolartoMol

    #[cell_parameters]
    # pa degeri 22 saat olarak hesaplanan proliferaton zamanin kac time step de bitecegini belirler.
    # ayni zamanda difuzyon denklemlerinde dt olarak kullanilan timeStepDuration parametresini belirler
    # or: pa = 10 icin dt = 1/10 = 0.1 olmaktadir.
    paStandart = 1

    # Proliferation time as hour
    proliferationTime = 22

    naturalApoptosisRate = 2 # Percent

    # Oxygen background concentration
    # Could also be validated with http://antoine.frostburg.edu/chem/senese/101/solutions/faq/predicting-DO.shtml
    o2Background = 0.21875 * coefficentForMiliMolartoMol
    #Glucose background concentration mM to mol/cm^2
    glucoseBackground = 17*coefficentForMiliMolartoMol
    #Hydrogen ion background concentration
    # Blood Flow, Metabolism, Cellular Microenvironment, and
    phBackground = 7.35
    HIyonBackGround = 1.116709 * 10**-13

    #o2prodRate = ((8.2 * 10**-3) * 3600*22)/ o2Background

    #[o2Config]
    #Diffusion Constant as cm**2/second
    # Since we calculate diffusion in a virtual cellRepresentationCoeff cm^2 area we divide glucoseDiffConst to cellRepresentationCoeff
    o2DiffConst = (8*10**-5)/cellRepresentationCoeff
    #Oxygen Uptake Rate Constant mol/(cell*second)

    #o2ConsRate = (0.91 * 10**-15)/60
    #o2ConsRate = 2.3*10**-16
    # https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3147247/
    #The Rate of Oxygen Utilization by Cells
    o2ConsRate = 2.5*10**-18

    hypoxiaInducedApoptosisTreshold = 0.015 * o2Background
    hypoxiaInducedGlycolysisTresholdMax = 0.05 * o2Background
    hypoxiaInducedGlycolysisTresholdMin = 0.01 * o2Background
    hypoxiaReceptorDelay = 7
    hypoxiaInducedApoptosisRate = 55 # Percent
    oxygenTresholdProliferativeToQuiscence = 1 * coefficentForO2mmHgToMol

    #[glucoseConfig]
    #Hypoglicemia induced apoptosis treshold mM to mol/cm^2
    apoptosisTresholdForGlucose = 8 * coefficentForMiliMolartoMol
    #Diffusion Constant as cm**2/second
    # Since we calculate diffusion in a virtual cellRepresentationCoeff cm^2 area we divide glucoseDiffConst to cellRepresentationCoeff
    glucoseDiffConst = (1.35 * 10**-5)/cellRepresentationCoeff
    #Glucose Consumption Rate mol/(cell*second)
    glucoseAerobicConsRate = 4.1 * 10**-17
    #glucoseAnaerobicConsRate = 16 * 4.1 * 10**-16
    # See: Hypoxia-inducible transcription factor promotes
    #hypoxia-induced A549 apoptosis via a mechanism that involves the
    #glycolysis pathway
    glucoseAnaerobicConsRate = 16 * 4.1 * 10**-17
    # mM to mol/cm^2
    glucoseTresholdProliferativeToQuiscence = 12 * coefficentForMiliMolartoMol

    #[hydrogenIyonConfig]
    phApoptosisTreshold = 6.5
    minPh = 5.5
    maxPh = 8.0
    optimalPh = 6.8
    #Diffusion Constant as cm**2/second
    hydrogenIyonDiffConst = 1.4 * 10**-6
    #hydrogenIyonDiffConst = 1.1*10**-5
    #Oxygen Consumption Rate mol/(cell*second)
    acidProdRate = 1.5 * 10**-18

    mutationDelay = 1
    migrationDelay = 10
    apoptosisDelay = 10

    hypoxiaInducedMigrationTreshold = hypoxiaInducedGlycolysisTresholdMax *5
    hypoglicemiaInducedMigrationTreshold = apoptosisTresholdForGlucose * 5
    phInducedMigrationTreshold = phApoptosisTreshold

    proliferationBorderSearchThreshold = 10
    cellStressScoreThreshold = 5

