__author__ = 'srb'

import math
import Cell


'''Finds border cells around the cell wtih in predefined radius (borderThreshold)'''
def getBorderCells(x,y,borderThreshold,borderCellMatrix):
    borderCellList = []
    for i in xrange(borderThreshold, -(borderThreshold+1), -1):
        for j in xrange(borderThreshold, -(borderThreshold+1), -1):
            # TODO this shouldn't be here
            if x+i < 400 and y+j < 400:
                if (x+i, y+j) not in borderCellList and borderCellMatrix[x+i][y+j]:
                    borderCellList.append((x+i, y+j))
    return borderCellList

def findMinLenghtToBorder(borderCellList, x, y):
    bCellX = borderCellList[0][0]
    bCellY = borderCellList[0][1]
    try:
        minLenghtToBorder = math.sqrt((bCellX - x)**2 + (bCellY-y)**2)
    except:
        print bCellX
        print bCellY
        raise
    for coordinate in borderCellList:
        bCellX = coordinate[0]
        bCellY = coordinate[1]
        try:
            l = math.sqrt((bCellX - x)**2 + (bCellY-y)**2)
        except:
            print bCellX
            print bCellY
            raise

        if l<minLenghtToBorder:
            minLenghtToBorder = l

    return minLenghtToBorder

def createNewCell(cells, cellStateMatrix, candidateCell,cellNextStateMatrix,cellTypeMatrix,coordinatesOfInvasionX,coordinatesOfInvasionY,
              cellMetabolismMatrix):
    if cellStateMatrix[coordinatesOfInvasionX][coordinatesOfInvasionY] != Cell.CellClass.APOPTOSIS:
        cellNextStateMatrix[coordinatesOfInvasionX][coordinatesOfInvasionY] = Cell.CellClass.QUIESCENCE
        cellTypeMatrix[coordinatesOfInvasionX][coordinatesOfInvasionY] = Cell.CellClass.TUMOR_CELL
        cellMetabolismMatrix[coordinatesOfInvasionX][coordinatesOfInvasionY] = cellMetabolismMatrix[candidateCell.cellPositionX][candidateCell.cellPositionY]
        candidateCell.age = 0
        # This part genetic inheritance transfered from ancestor to new cell. we copy ancestors list to new cell
        cells[coordinatesOfInvasionX][coordinatesOfInvasionY].dna  = list(candidateCell.dna)
        return True
    else:
        return False
