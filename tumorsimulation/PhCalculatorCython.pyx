__author__ = 'Serbulent Unsal'

import cython

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef double convertHIyonConcentrationToPH(double hIyonConcentration,  double coefficentForMiliMolartoMol) :
    cdef double L = 1.0

    """This function converts H+ concentration to ph value.
    :concentration: height width and lenght of area is based on configuration.
    """
    cdef double ph
    cdef extern from "math.h":
        double log10(double x) nogil
    ph = -log10((hIyonConcentration/0.0025 * L)*1000)
#    ph =  3 + log10(coefficentForMiliMolartoMol/hIyonConcentration)
    return ph

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef double convertPhtoHIyonConcentration(double ph, double coefficentForMiliMolartoMol) :
    cdef double L = 3.0

    """This function converts  ph value to H+ concentration as mol/cm^2.
        :ph: height width and lenght of area is based on configuration.
    """
    cdef double concentration
    cdef extern from "math.h":
        double pow(double x, double y) nogil

    concentration = pow(10,-ph) * 1000 * coefficentForMiliMolartoMol

    #concentration = (pow(10,-ph)/1000)*0.0025 * L
    return concentration
