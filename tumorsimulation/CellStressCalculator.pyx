__author__ = 'Serbulent Unsal'

import cython
from libc.stdlib cimport rand

import CellUtils

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
def calculateCellStress(cells, int x, int y, borderThreshold, stressScoreMatrix,borderCellMatrix):

    borderCellList = CellUtils.getBorderCells(cells,  x,  y, borderThreshold,borderCellMatrix)

    if len(borderCellList) > 0:
        stressScoreMatrix[x][y] = CellUtils.findMinLenghtToBorder(borderCellList, x, y) + 1.0
    else:
        stressScoreMatrix[x][y] = 99

    return 1