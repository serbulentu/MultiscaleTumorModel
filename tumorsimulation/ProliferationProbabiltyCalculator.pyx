__author__ = 'Serbulent Unsal'

import cython
cimport  PhCalculatorCython
from libc.stdlib cimport rand

cdef int AEROBIC = 1
cdef int ANAEROBIC = 2

# willCellProliferationOccurs is using to guess will cell proliferate or not in a probabilistic way
# if all conditions are optimal the probability is %100 percent
@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
def willCellProliferationOccurs( int candidateCellMetabolism, double  o2Concentration,\
                                 double  glucoseConcentration,double  acidConcentration, \
                                 double candidateCellProliferationGeneticEffect, \
                                 double o2BackgroundConcentration, double apoptosisTresholdForO2, \
                                 double glucoseBackgroundConcentration,double apoptosisTresholdForGlucose, \
                                 double optimalPh, double phBackground, double minPh, double maxPh,\
                                 float cpo, float cpg, float cph, float cpg_an, float cph_an,double coefficentForMiliMolartoMol):


    cdef double o2Percentage = calculateO2ProliferationProbability(o2Concentration,o2BackgroundConcentration,apoptosisTresholdForO2)
    cdef double glucosePercentage = calculateGlucoseProliferationProbability(glucoseConcentration,glucoseBackgroundConcentration,apoptosisTresholdForGlucose)
    cdef double acidConcentrationWithBackground = (acidConcentration+1)
    cdef double phPercentage = calculatePhProliferationProbability(acidConcentrationWithBackground,optimalPh, phBackground, minPh, maxPh,coefficentForMiliMolartoMol)
    cdef double weightedProliferationProbality = 0

    #This means glucose concentration is below apoptosisthreshold
    #Then there will be no proliferation
    if glucosePercentage < 0:
        return False

    glucoseReductionCoeff = 1
    if glucosePercentage < 20:
        glucoseReductionCoeff = 4 

    if candidateCellMetabolism == AEROBIC:
        weightedProliferationProbality = (cpo * o2Percentage) + (cpg * glucosePercentage) + (cph * phPercentage)
    else:
        weightedProliferationProbality = (cpg_an * glucosePercentage) + (cph_an * phPercentage)

    cdef int chance = rand() %100
    weightedProliferationProbality = (weightedProliferationProbality +\
                                      candidateCellProliferationGeneticEffect)/glucoseReductionCoeff
    if weightedProliferationProbality > chance:
        return True
    else:
        return False

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double calculateO2ProliferationProbability(double o2Concentration, double o2BackgroundConcentration, double apoptosisTresholdForO2) :
    # o2ConcentrationReal = o2Concentration * o2BackgroundConcentration
    # o2 One Percent is (o2BackgroundConcentration - cap)/100
    cdef double o2ProliferationProbability = ((o2Concentration * o2BackgroundConcentration) - apoptosisTresholdForO2)/((o2BackgroundConcentration - apoptosisTresholdForO2)/100)
    #print 'o2ProliferationProbability: ' + str(o2ProliferationProbability)
    return o2ProliferationProbability

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double calculateGlucoseProliferationProbability(double glucoseConcentration,double glucoseBackgroundConcentration,double apoptosisTresholdForGlucose) :
    cdef double glucoseProliferationProbability =\
            ((glucoseConcentration * glucoseBackgroundConcentration)- apoptosisTresholdForGlucose)/((glucoseBackgroundConcentration - apoptosisTresholdForGlucose)/100)
    return glucoseProliferationProbability

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double calculatePhProliferationProbability(double acidConcentration,double optimalPh, double phBackground, double minPh, double maxPh, double coefficentForMiliMolartoMol): #FIND a way to use nogil for convertPhtoHIyonConcentration

    cdef double tmpIyonConc = PhCalculatorCython.convertPhtoHIyonConcentration(phBackground,coefficentForMiliMolartoMol)
    cdef double acidConcentrationReal = acidConcentration * tmpIyonConc
    cdef double phOfCellEnviroment = PhCalculatorCython.convertHIyonConcentrationToPH(acidConcentrationReal,coefficentForMiliMolartoMol)
    cdef double phOnePercent = 0.0
    cdef double phProliferationProbability = 0.0

    cdef extern from "math.h":
        double fabs(double x) nogil

    if phOfCellEnviroment < optimalPh:
        phOnePercent = (optimalPh - minPh)/100
        phProliferationProbability = phOfCellEnviroment-minPh/phOnePercent
    elif phOfCellEnviroment > optimalPh:
        phOnePercent =fabs(optimalPh - maxPh)/100
        phProliferationProbability = fabs(phOfCellEnviroment-maxPh)/phOnePercent
  #  elif phOfCellEnviroment < minPh or phOfCellEnviroment > maxPh:
  #      return 0
    elif phOfCellEnviroment == optimalPh:
        return 100

    return 0 #phProliferationProbability
