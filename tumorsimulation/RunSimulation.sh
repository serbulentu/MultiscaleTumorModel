#!/bin/bash

rm *.pyc
rm images/*
rm statistics/*
rm *.so
python setup.py build_ext --inplace
cp __init__.py tumorsimulation/
cp tumorsimulation/*.so .
python2 SimulationMain.py
