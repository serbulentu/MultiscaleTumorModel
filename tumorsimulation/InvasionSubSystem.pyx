# cython: profile=True

import cython
import math
cimport  PhCalculatorCython
from random import randint

from libc.stdlib cimport rand
from Cell import CellClass
from ConfigModule import ConfigClass

cdef int AEROBIC = CellClass.AEROBIC
cdef int ANAEROBIC = CellClass.ANAEROBIC
cdef int EMPTY = 0

cdef int PROLIFERATION = 1
cdef int MIGRATION = 2

cdef float optimalAcidConcentration = ConfigClass.optimalPh

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
def findMostSuitableNeighbourToInvade(cellStressScoreMatrix, cellTypeMatrix, int cellMetabolism, int x, int y, \
                                      o2ConcentrationMatrix, glucoseConcentrationMatrix, \
                                      acidConcentrationMatrix, phBackground, \
                                      int proliferationStressTreshold, int timeStep, int invasionCause,\
                                      float cmo, float cmg, float cmh, float
                                      cmg_an, float cmh_an,double
                                      coefficentForMiliMolartoMol, int isTest):
    cdef int invasionX = 0
    cdef int invasionY = 0
    cdef double maxInvasionScore = 0
    cdef double minStressScore = 999
    cdef double invasionConvenienceScore = 0
    cdef int candidateCellMetabolism = cellMetabolism
    cdef int xTarget = 0
    cdef int yTarget = 0
    cdef int tmpCordinate1D = 0
    cdef double o2Concentration = 0
    cdef double glucoseConcentration = 0
    cdef double hIyonConcentration = 0
    cdef double optimalAcidityScoreForInvasion = 0
    cdef dict invasionCandidates = {}
    cdef dict stressScoreDict = {}
    # If a cell is empty then it has priority
    cdef dict emptyCandidates = {}
    cdef timeStepType = timeStep % 2
    cdef int process = 0

    cdef int maxRange = 0
    cdef int minRange = 0

    cdef int sizeOfMatrix = 0
    sizeOfMatrix = math.sqrt(len(o2ConcentrationMatrix))

    #Search all neighbours in a 3x3 surrounding grid
    cdef int i, j

    cdef extern from "math.h":
        double fabs(double numberForAbs) nogil

    if invasionCause == PROLIFERATION:
        maxRange = 1
        minRange = -2
    elif invasionCause == MIGRATION:
        if isTest:
            maxRange = 1 
            minRange = -2
        else:
            maxRange = 9 
            minRange = -10

    for i in xrange(maxRange, minRange, -1):
        for j in xrange(maxRange, minRange, -1):
            # Check orthogonal cells in even time steps and diagonal ones in odd time steps
            # With process variable we ensure that invasion candidates assigned only if xTarget and yTarget assigned
            if invasionCause == PROLIFERATION:
                if timeStepType == 0:
                    if i == 0 or j == 0:
                        xTarget = x + i
                        yTarget = y + j
                        process = 1
                    else:
                        process = 0
                elif timeStepType == 1:
                    if i != 0 and j != 0:
                        xTarget = x + i
                        yTarget = y + j
                        process = 1
                    else:
                        process = 0
            elif invasionCause == MIGRATION:
                xTarget = x + i
                yTarget = y + j
                process = 1
            
            #We only search in neighbour cells don't want to consider cell itself
            #So we'd like to sure that i and j are not equal to 0 at the same time
            
            if (i, j) != (0, 0) and process == 1 and xTarget<sizeOfMatrix and yTarget<sizeOfMatrix:
                process = 0
                tmpCordinate1D = (xTarget *  sizeOfMatrix) + yTarget
                o2Concentration = float(returnZeroIfNegative(o2ConcentrationMatrix[tmpCordinate1D]))
                glucoseConcentration = float(returnZeroIfNegative(glucoseConcentrationMatrix[tmpCordinate1D]))
                # TODO access convertPhtoHIyonConcentration in a cythonic way (associated with nogil problem)
                # TODO I also do not remember why I write +1 at the line below
                hIyonConcentration = (acidConcentrationMatrix[tmpCordinate1D] + 1)\
                                     * PhCalculatorCython.convertPhtoHIyonConcentration(phBackground,coefficentForMiliMolartoMol)
                optimalAcidityScoreForInvasion = returnZeroIfNegative(getOptimalAcidityScoreForInvasion(optimalAcidConcentration,
                                                                       hIyonConcentration, coefficentForMiliMolartoMol))

#               Same score for all cases
                invasionConvenienceScore = cmo * o2Concentration + cmg * glucoseConcentration + cmh * optimalAcidityScoreForInvasion
                # Save all invasionCanditeCell coordintes and invasionScore
                invasionCandidates[(xTarget, yTarget)] = invasionConvenienceScore
#                print invasionCandidates
                if cellTypeMatrix[xTarget][yTarget] == CellClass.EMPTY:
                    emptyCandidates[(xTarget, yTarget)] = 0
                else:
                    stressScoreDict[(xTarget, yTarget)] = cellStressScoreMatrix[xTarget][yTarget]

                if cellStressScoreMatrix[xTarget][yTarget] < minStressScore:
                    minStressScore = cellStressScoreMatrix[xTarget][yTarget]

                if invasionConvenienceScore > maxInvasionScore:
                    maxInvasionScore = invasionConvenienceScore

    # Delete cell from invasion candidate list if invasion  score difference is more than 0.1 (Max score can be 1)
    # Deleting candidate only from stressScoreDict is enough
#    print "Empty Can: " + str(emptyCandidates)
#    print "Stress: " + str(stressScoreDict)
#    print "Cell Type: " + str(cellTypeMatrix)
    if len(emptyCandidates) == 0:
        for coordinate, invasionScore in invasionCandidates.items():
            if invasionScore != maxInvasionScore and fabs(maxInvasionScore - invasionScore) > 0.1:
                del stressScoreDict[coordinate]
                # Delete cell from invasion candidate list if stress score difference is more than 1
        for coordinate, stressScore in stressScoreDict.items():
            if stressScore != minStressScore and fabs(minStressScore - stressScore) > 1:
                del stressScoreDict[coordinate]

        if len(stressScoreDict) == 0:
            print 'no invasion candidate found'
            return (x,y)
        elif len(stressScoreDict) == 1:
            invasionCordinates = stressScoreDict.iterkeys().next()
            return invasionCordinates
        else:
            coordinateList = stressScoreDict.keys()
            randomIndex = randint(0, len(coordinateList) - 1)
            coordinatesofEasiestCellForInvasion = coordinateList[randomIndex]
            return coordinatesofEasiestCellForInvasion
    else:
        if len(emptyCandidates) == 1:
            invasionCordinates = emptyCandidates.iterkeys().next()
            return invasionCordinates
        else:
            # Delete cell from invasion candidate list if invasion  score
            # difference is more than 0.1 (Max score can be 1)
            for coordinate, invasionScore in invasionCandidates.items():
                if invasionScore != maxInvasionScore and fabs(maxInvasionScore - invasionScore) > 0.1:
                    del invasionCandidates[coordinate]
#            print "invasion: " + str(invasionCandidates)
            coordinateList = invasionCandidates.keys()
            randomIndex = randint(0, len(coordinateList) - 1)
            coordinatesofEasiestCellForInvasion = coordinateList[randomIndex]
            return coordinatesofEasiestCellForInvasion

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double returnZeroIfNegative(double x):
    if x < 0:
        return 0.0
    else:
        return x

#Find candidateCellAreaAcidConcentration is how close to OptimalPh
# This function defined with cpdef for testing purposes
@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef double getOptimalAcidityScoreForInvasion(double candidateCellOptimalPh, double candidateCellAreaAcidConcentration,
                                              double coefficentForMiliMolartoMol):
    #Print lines only for debug purposes
    #print "candidateCellOptimalPh: " + str(candidateCellOptimalPh)
    #print "candidateCellAreaAcidConcentration: " + str(candidateCellAreaAcidConcentration)
    #print "coefficentForMiliMolartoMol: " + str(coefficentForMiliMolartoMol)

   # print "ph: " + str(
   #     PhCalculatorCython.convertHIyonConcentrationToPH(candidateCellAreaAcidConcentration,coefficentForMiliMolartoMol)).strip()

    cdef extern from "math.h":
        double fabs(double x) nogil

    cdef double phDiff = fabs(
        candidateCellOptimalPh - PhCalculatorCython.convertHIyonConcentrationToPH(candidateCellAreaAcidConcentration,coefficentForMiliMolartoMol))
    cdef double err = 0.1
    if phDiff <= err:
        return 1
    else:
        # We would like to minimum score gets maximum score so we use 1/phDiff
        # instead of phDiff
        # 1/phDiff will be maximum 10 in case if phDiff equals 0.1 otherwise 
        # phDiff will not go beyond 3 according to literature so min 1/phDiff
        # will be 0.3
        return ((1 / phDiff) - 0.3) / (10 - 0.3)
