__author__ = 'Serbulent Unsal'

import tumorsimulation.PhCalculatorCython as PhCalculatorCython

from Cell import CellClass
from random import randint

class MigrationPhenotypeDecisionSystemClass():

    def __init__(self,configObject):
        self.configObject = configObject
        self.migrativeCellNumber = 0
        if self.configObject.statisticsDebug:
            with open('./statistics/invasiveCellNumber.csv', 'w') as cellNumberLog:
                cellNumberLog.write('Time;Invasive Cell Number\n')
        self.oldTimeStep = 0

    def decideForMigration(self,targetCell,cellNextStateMatrix,o2Concentration, glycoseConcentration,\
                                hIyonConcentration, migrativePhenotypeProbabilityMatrix, timeStep):
        if self.configObject.statisticsDebug:
            if timeStep > self.oldTimeStep:
                self.oldTimeStep = timeStep
                with open('./statistics/migrativeCellNumber.csv', 'a') as cellNumberLog:
                    cellNumberLog.write(str(timeStep) +';'+ str(self.migrativeCellNumber) + '\n')
                self.migrativeCellNumber = 0

        if timeStep > 0:

#            print(" glycoseConcentration: " + str( glycoseConcentration))
#            print("self.configObject.hypoglicemiaInducedMigrationTreshold: " + str(self.configObject.hypoglicemiaInducedMigrationTreshold))
            # We assume that hypoxia starts at hypoxiaInducedGlycolysisTresholdMax
            if  o2Concentration < \
                self.configObject.hypoxiaInducedMigrationTreshold: #and \
            #o2Concentration > self.configObject.hypoxiaInducedApoptosisTreshold:
              #  print("o2Concentration: " + str(o2Concentration))
              #  print("self.configObject.hypoxiaInducedMigrationTreshold: " +  str(self.configObject.hypoxiaInducedMigrationTreshold) )
              #  print(" self.configObject.hypoxiaInducedApoptosisTreshold:" + str(self.configObject.hypoxiaInducedApoptosisTreshold))

                # If o2Concentration = 0 then hypoxiaInducedMigrationProbability should %100
                # If o2Concentration = hypoxiaInducedMigrationTreshold then hypoxiaInducedMigrationProbability should %0
                # and between them it should increases when o2Concentration decreases from hypoxiaInducedMigrationTreshold to 0
                hypoxiaInducedMigrationProbability = ((self.configObject.hypoxiaInducedMigrationTreshold - o2Concentration)/ self.configObject.hypoxiaInducedMigrationTreshold ) * 100
                migrativePhenotypeProbabilityMatrix[targetCell.cellPositionX][targetCell.cellPositionY] = hypoxiaInducedMigrationProbability
                chance = randint(1,100)
                if hypoxiaInducedMigrationProbability > chance:# or o2Concentration <= 0:
                    return self.startMigration(targetCell,cellNextStateMatrix)

            # check hypoglycemia induced migration between hypoglycemia treshold and zero
            # we increase migration probability %1 for each portion
            if glycoseConcentration <  self.configObject.hypoglicemiaInducedMigrationTreshold:
#                    and glycoseConcentration >            self.configObject.apoptosisTresholdForGlucose/4:
                reductionCoeff = 1
                # lets reduce migration probability when glucose level decrease
                if  glycoseConcentration <                self.configObject.hypoglicemiaInducedMigrationTreshold/10:
                    hypoglycemiaInducedMigrationProbability = \
                        ((self.configObject.hypoglicemiaInducedMigrationTreshold  -  glycoseConcentration)/ \
                        self.configObject.hypoglicemiaInducedMigrationTreshold)  *  100
                    hypoglycemiaInducedMigrationProbability = 100 -  hypoglycemiaInducedMigrationProbability
                else:
                    hypoglycemiaInducedMigrationProbability = \
                        ((self.configObject.hypoglicemiaInducedMigrationTreshold  -  glycoseConcentration)/ \
                        self.configObject.hypoglicemiaInducedMigrationTreshold)  *  100
                migrativePhenotypeProbabilityMatrix[targetCell.cellPositionX][targetCell.cellPositionY]  = hypoglycemiaInducedMigrationProbability

#                print(" glycoseConcentration: " + str( glycoseConcentration))
#                print("self.configObject.hypoglicemiaInducedMigrationTreshold: " + str(self.configObject.hypoglicemiaInducedMigrationTreshold))
#                print(" hypoglycemiaInducedMigrationProbability: " + str(hypoglycemiaInducedMigrationProbability))
                chance = randint(1,100)
                if hypoglycemiaInducedMigrationProbability > chance or glycoseConcentration <= 0:
                    return self.startMigration(targetCell,cellNextStateMatrix)
            # If migration not started return False
            return False
#            cellMicroenviromentPh = PhCalculatorCython.convertHIyonConcentrationToPH(hIyonConcentration, self.configObject.coefficentForMiliMolartoMol)
#            if cellMicroenviromentPh <= self.configObject.phInducedMigrationTreshold:
#                onePercent = (self.configObject.phInducedMigrationTreshold -self.configObject.minPh)/100
#                acidInducedMigrationProbability = (cellMicroenviromentPh - self.configObject.minPh)/onePercent
#                acidInducedMigrationProbability -= targetCell.apoptosisEffect
#                survivalPossibility = randint(1,100)
#                if acidInducedMigrationProbability > survivalPossibility:
#                    return self.startMigration(targetCell,cellNextStateMatrix)
#            elif PhCalculatorCython.convertHIyonConcentrationToPH(hIyonConcentration,self.configObject.coefficentForMiliMolartoMol) >= self.configObject.maxPh:
#                return self.startMigration(targetCell,cellNextStateMatrix)

    def startMigration(self, targetCell,cellNextStateMatrix):
        targetCell.migrationDelayCounter += 1
        chanceForMigration = randint(1,self.configObject.migrationDelay)
        if targetCell.migrationDelayCounter > chanceForMigration:
            cellNextStateMatrix[targetCell.cellPositionX][targetCell.cellPositionY] = CellClass.MIGRATIVE
            self.migrativeCellNumber += 1
            return True
