__author__ = 'Serbulent Unsal'

import CellUtils
import tumorsimulation.InvasionSubSystem as InvasionSubSystem
from Cell import CellClass
from CellShifter import CellShifterClass


class MigrationSubSystemClass():

    def __init__(self, configObject, migrationPhenotypeDecisionSystemClass, cmo, cmg, cmh, cmg_an, cmh_an):
        self.configObject = configObject
        self.migrationPhenotypeDecisionSystem = migrationPhenotypeDecisionSystemClass
        self.cmo = cmo
        self.cmg = cmg
        self.cmh = cmh
        self.cmg_an = cmg_an
        self.cmh_an = cmh_an

        csc = CellShifterClass(cmo, cmg, cmh, cmg_an, cmh_an)
        self.shiftCellsStartFrom = csc.shiftCellsStartFrom

    def willCellMigrate(self, cells,cellStateMatrix, cellStressScoreMatrix, targetCell, cellTypeMatrix, cellNextStateMatrix, cellMetabolismMatrix,
                        o2Concentration, glucoseConcentration, hIyonConcentration,
                        o2ConcentrationMatrix, glucoseConcentrationMatrix, acidConcentrationMatrix,
                        migrativePhenotypeProbabilityMatrix, borderCellMatrix,
                        timeStep, o2consumptionMatrix,
                        glucoseConsumptionMatrix, acidProductionMatrix):

        if self.migrationPhenotypeDecisionSystem.decideForMigration(targetCell, cellNextStateMatrix, o2Concentration, glucoseConcentration,
                                                                    hIyonConcentration, migrativePhenotypeProbabilityMatrix, timeStep):

            coordinatesOfInvasion = InvasionSubSystem.findMostSuitableNeighbourToInvade(cellStressScoreMatrix, cellTypeMatrix,
                                                                                        cellMetabolismMatrix[targetCell.cellPositionX][
                                                                                            targetCell.cellPositionY], targetCell.cellPositionX, targetCell.cellPositionY, o2ConcentrationMatrix,
                                                                                        glucoseConcentrationMatrix, acidConcentrationMatrix,
                                                                                        self.configObject.phBackground,
                                                                                        self.configObject.cellStressScoreThreshold, timeStep, 2,
                                                                                        self.cmo, self.cmg, self.cmh, self.cmg_an, self.cmh_an,
                                                                                        self.configObject.coefficentForMiliMolartoMol, 0)

            x = coordinatesOfInvasion[0]
            y = coordinatesOfInvasion[1]
            # Be sure that there is a valid invasion candidate
            if (x,y) != (targetCell.cellPositionX,targetCell.cellPositionY):
                if cellTypeMatrix[x][y] == CellClass.EMPTY:
                    isCellCreated = CellUtils.createNewCell(cells, cellStateMatrix, targetCell, cellNextStateMatrix,
                                            cellTypeMatrix,coordinatesOfInvasion[0],coordinatesOfInvasion[1],cellMetabolismMatrix)
                    if isCellCreated:
                        cellTypeMatrix[targetCell.cellPositionX][targetCell.cellPositionY] = CellClass.EMPTY
                        cellStateMatrix[targetCell.cellPositionX][targetCell.cellPositionY] = CellClass.EMPTY
                        cellNextStateMatrix[targetCell.cellPositionX][targetCell.cellPositionY] = CellClass.EMPTY
                        targetCell.setParametersToZero(o2consumptionMatrix, glucoseConsumptionMatrix, \
                                         acidProductionMatrix, migrativePhenotypeProbabilityMatrix,
                                         targetCell.cellPositionX, targetCell.cellPositionY)
                   # print 'Migrative Cell' + str((targetCell.cellPositionX, targetCell.cellPositionY))
                   # print 'coordinatesOfInvasion' + str(coordinatesOfInvasion)
                   # print 'Cell Migrated'
#                else:
#                    # print 'coordinatesOfInvasion: ' + str(coordinatesOfInvasion)
#                    # print  'Error on migration, no empty neighbour found'
#                    processedCellList = []
#                    self.shiftCellsStartFrom(cells,cellStateMatrix, cellStressScoreMatrix, cells[coordinatesOfInvasion[0]][coordinatesOfInvasion[1]], cellMetabolismMatrix, processedCellList,
#                                             o2ConcentrationMatrix, glucoseConcentrationMatrix,
#                                             acidConcentrationMatrix, cellNextStateMatrix, cellTypeMatrix,
#                                             borderCellMatrix, timeStep, self.configObject, 2)
#                return True
            else:
                return False
