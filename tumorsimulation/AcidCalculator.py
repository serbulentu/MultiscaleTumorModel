''' H+ diffusion calculator '''

__author__ = 'Serbulent Unsal'

# pylint:disable=import-error,too-many-public-methods,invalid-name,attribute-defined-outside-init,wrong-import-position,wrong-import-order, wildcard-import
from fipy import *
import numpy as np

class AcidCalculatorClass:

    ''' H+ diffusion calculator '''
    def __init__(self,nonDimensionaliser,configObject):

        self.conf = configObject

        nx = configObject.nx
        cellSize = configObject.cellSize
        nx = nx/cellSize
        dx = configObject.dx_real
        ny = nx
        dy = dx
        mesh = Grid2D(dx=dx, dy=dy, nx=nx, ny=ny)
        self.phi = CellVariable(name="H+ Concentration", mesh=mesh, value = 0.0 )

        self.nonDimensionalizer = nonDimensionaliser
        D = self.nonDimensionalizer.nonDimensionalHIyonDiffConst

        border = 1.0
        self.phi.constrain(border, mesh.facesLeft)
        self.phi.constrain(border, mesh.facesRight)
        self.phi.constrain(border, mesh.facesTop)
        self.phi.constrain(border, mesh.facesBottom)

        proliferationAge = configObject.paStandart
        self.timeStepDuration = 1/proliferationAge

        self.viewer = Viewer(vars=self.phi)

        self.source = CellVariable(mesh=mesh)
        self.large_value = CellVariable(mesh=mesh)
        self.sourceCapilary = CellVariable(mesh=mesh)
        self.eq = TransientTerm() == DiffusionTerm(coeff=D) + self.source 

        #import fipy.solvers.trilinos as solvers
        #self.solver = solvers.linearPCGSolver.LinearPCGSolver(precon=None, iterations=40, tolerance=1e-15)

    def calculateAcid(self, sourceTerm,timeStep,isTest):

        self.source.setValue(sourceTerm)
        self.eq.solve(var=self.phi, dt=self.timeStepDuration)
        willImagesSaved = self.conf.save_images
        if isTest is False and willImagesSaved and timeStep%self.conf.imageSaveInterval == 0:
            nameofpic = "./images/hIyonDiffusion_" + str(timeStep) + ".png"
            self.viewer.plot(filename=nameofpic)
        else:
            self.viewer.plot()
        #TSVViewer(vars=(phi)).plot(filename="output.txt")

        # globalValue
        # Concatenate and return values from all processors
        # When running on a single processor, the result is identical to value.

        return self.phi.globalValue;
